Geoverse::Application.routes.draw do

  namespace :v1 do
    resource :session
    resource :registration
    resource :password
    resources :bibles
    resources :dropped_verses
    resources :nearby_verses
  end

  match 'mobile/v1/screens/:action.html' => 'mobile/v1/screens', :as => :mobile_v1_screen
end
