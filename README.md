== geoverse

I'll write some awesome readme later...

== Special Notes

When setting up project, make sure to have qmake installed:
`brew install qt --compile-from-source`

== Phonegap Build Steps

1. Create the html files by running `export_app=production rspec spec/requests/mobile/v1/export_views_spec.rb`

2. Run `rake assets:clean assets:precompile` to generate the assets

3. `cp config.xml.example mobile_builds/config.xml`

4. Update the version information inside the config.xml

5. Move the jquery mobile images, leaflet images, css, js and mobile
   icons into an 'assets' folder. The other files need to be one level up
from those. example:
  mobile_builds/
    html files
    phonegap config
    assets/
      icons
      js/css
      vendor images

6. Remove the /assets/ prefix from all images in the css files

7. Remove the / from the images in the js files

8. Remove the https protocol for the drop a verse, my verses and nearby verses pages

9. Verify that a page shows the correct styling and images

10. Zip everything and send it to phonegap

11. Go to disneyland... on an odd day

== Android Dev Notes

To install an apk file on an Android:

1. cd into the android sdk platform tools directory

2. Connect the device

3. Run `./adb install path/to/apk`
