class V1::PasswordsController < V1::ApplicationController
  before_filter :authenticate, :only => :update

  def create
    user = User.where(email: params[:email]).first

    response_hash = if user.present?
      user.create_and_save_new_password
      render :json => {}
    else
      render :json => {
        :errors => "Hmmm, we couldn't find the email address you provided. Try again?"
      }, :status => 422
    end
  end

  def update
    if current_user.update_attributes(params[:user])
      render :json => {}
    else
      render :json => {
        :errors => current_user.errors.full_messages
      }, :status => 422
    end
  end
end
