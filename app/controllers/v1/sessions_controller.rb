class V1::SessionsController < V1::ApplicationController
  before_filter :downcase_email, :only => :create

  def create
    user = User.where(email: @downcased_email).first
    if user && user.authenticate(params[:password])
      render :json => {
        :auth_token => user.create_and_save_new_auth_token,
        :forgot_password => user.has_forgot_password?
      }
    else
      render :json => {
        :errors => "Sorry! We couldn't find your email or password."
      }, :status => 422
    end
  end

  private

  def downcase_email
    email = params[:email]
    @downcased_email = email.downcase if email.present?
  end
end
