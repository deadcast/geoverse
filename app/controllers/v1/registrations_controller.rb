class V1::RegistrationsController < V1::ApplicationController
  def create
    user = User.new(params[:user])
    if user.save
      render :json => {
        :auth_token => user.create_and_save_new_auth_token
      }, :status => 201
    else
      render :json => {
        :errors => user.errors.full_messages
      }, :status => 422
    end
  end
end
