class V1::ApplicationController < ActionController::Base
  def current_user
    @current_user ||= User.where(:authentication_token => params[:auth_token]).first
  end

  def authenticate
    if current_user.blank?
      render :json => {}, :status => 401
    end
  end
end
