class V1::BiblesController < V1::ApplicationController
  before_filter :authenticate

  def index
    render json: Bible.lookup(params[:lookup])
  end
end

