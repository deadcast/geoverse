class V1::NearbyVersesController < V1::ApplicationController
  before_filter :authenticate

  def index
    render json: {
      verses: DroppedVerse.nearby(params[:lat], params[:lon])
    }
  end

  def show
    dropped_verse = DroppedVerse.find(params[:id])
    render json: {
      title: dropped_verse.title,
      text: dropped_verse.text,
      location: dropped_verse.location
    }
  end
end

