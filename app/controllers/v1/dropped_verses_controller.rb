class V1::DroppedVersesController < V1::ApplicationController
  before_filter :authenticate

  def index
    render json: { verses: current_user.verse_list }
  end

  def show
    dropped_verse = current_user.dropped_verses.find(params[:id])
    render json: {
      title: dropped_verse.title,
      text: dropped_verse.text,
      location: dropped_verse.location
    }
  end

  def create
    current_user.dropped_verses.create(
      verse: Verse.find(params[:lookup]),
      location: [params[:lat].to_f, params[:lon].to_f]
    )
    render json: {}, status: 201
  end
end

