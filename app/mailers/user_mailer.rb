class UserMailer < ActionMailer::Base
  default from: 'hello@geoverseapp.com'

  def forgot_password_email param_hash
    @new_password = param_hash[:new_password]
    mail(
      :to => param_hash[:email],
      :subject => 'Your shiny new password!'
    )
  end
end
