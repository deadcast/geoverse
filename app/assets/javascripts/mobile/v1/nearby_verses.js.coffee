document.addEventListener(
  'resume',
  (->
    if($('#nearby-verses-page:visible').length)
      $('#nearby-verses-page').trigger('pageinit')),
  false
)

$(document).delegate '.refresh', 'click', ->
  $('#nearby-verses-page').trigger('pageinit')

$(document).delegate '#nearby-verses-page', 'pageinit', ->
  $this = $(this)
  $droppedVersesList = $('#nearby-verses-list')
  $droppedVerseDetails = $('#nearby-verse-details')
  verseTemplate = "<li><a href='#' class='verse-details' data-verse-id='<%= id %>'><h1 class='title'><%= title %></h1><p class='time-ago'><%= time_ago %></p><p class='distance'><%= distance %></p></a></li>"
  verseDetailsUrl = $droppedVersesList.attr('data-url')
  noVersesError = "Oh my! Nobody has dropped any verses in your area. Let's fix that!"

  $('.notice, ' + $droppedVersesList.selector, $this).empty()

  success = (position) ->
    geoverse.helpers.ajax
      url: verseDetailsUrl
      data:
        lat: position.coords.latitude
        lon: position.coords.longitude
      success: (data) ->
        verses = JSON.stringify(data.verses)
        $('.notice', $this).text(noVersesError) if _.isEmpty(data.verses)

        $.each data.verses, (index, verse) ->
          $droppedVersesList.append _.template(verseTemplate, verse)

        $droppedVersesList.listview('refresh')

        $('li a', $droppedVersesList).click ->
          verse = JSON.parse(verses)[$(this).closest('li').index()]
          localStorage.verses = JSON.stringify([verse])
          $.mobile.changePage('view_on_map.html')

  error = ->
    geoverse.helpers.showError(geoverse.locationError)
    $('.notice', $this).text(noVersesError)

  geoverse.helpers.getLocation(success, error)

