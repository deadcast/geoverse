$(document).delegate '#my-verses-page', 'pagehide', -> $('.view-verse-map a').removeClass('ui-btn-active')
  
$(document).delegate '#my-verses-page', 'pageinit', ->
  $this = $(this)
  $droppedVersesList = $('#dropped-verses-list')
  $droppedVerseDetails = $('#dropped-verse-details')
  verseTemplate = "<li><a href='#' class='verse-details' data-verse-id='<%= id %>'><h1 class='title'><%= title %></h1><p class='time-ago'><%= time_ago %></p></a></li>"
  verseDetailsUrl = $droppedVersesList.attr('data-url')
  $('.view-verse-map').hide()

  geoverse.helpers.ajax
    url: verseDetailsUrl
    data: {}
    success: (data) ->
      verses = JSON.stringify(data.verses)
      if _.isEmpty(data.verses)
        $('.notice', $this).text("Oh noes! You haven't dropped any verses yet.")
      else
        $('.view-verse-map').show()

      $.each data.verses, (index, verse) ->
        $droppedVersesList.append _.template(verseTemplate, verse)

      $droppedVersesList.listview('refresh')
      
      $('.view-verse-map', $this).click ->
        localStorage.verses = verses
        $.mobile.changePage('view_on_map.html', { transition: 'slideup' })

      $('li a', $droppedVersesList).click ->
        verse = JSON.parse(verses)[$(this).closest('li').index()]
        localStorage.verses = JSON.stringify([verse])
        $.mobile.changePage('view_on_map.html')
