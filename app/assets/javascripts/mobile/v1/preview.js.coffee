$(document).delegate '#preview-page', 'pageinit', ->
  $('h1.title', this).text(localStorage.verseTitle)
  $('p.text', this).text(localStorage.verseText)
