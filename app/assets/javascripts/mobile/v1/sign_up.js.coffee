$(document).delegate '#sign-up-page', 'pageinit', ->
  $(this).delegate '#sign-up-form', 'submit', ->
    $this = $(this)
    geoverse.helpers.ajax
      url:  $this.attr('action')
      data:
        user:
          email: $('#sign-up-email', $this).val()
          password: $('#sign-up-password', $this).val()
          password_confirmation: $('#sign-up-password-confirmation', $this).val()
      type: $this.attr('method')
      success: (data) ->
        localStorage.authToken = data.auth_token
        $.mobile.changePage 'home.html'
      error: (data) ->
        errors = $('.errors', $this)
        errors.empty()
        $.each(data.errors, (index, value) ->
          errors.append $('<div/>').text(value))

    return false

