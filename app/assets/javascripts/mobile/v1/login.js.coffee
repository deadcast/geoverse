$(document).delegate '#login-page', 'pageinit', ->
  $(this).delegate '#login-form', 'submit', ->
    $this = $(this)
    geoverse.helpers.ajax
      url:  $this.attr('action')
      data:
        email: $('#login-email', $this).val()
        password: $('#login-password', $this).val()
      type: $this.attr('method')
      success: (data) ->
        localStorage.authToken = data.auth_token
        if data.forgot_password == true
          $.mobile.changePage 'update_password.html'
        else
          $.mobile.changePage 'home.html'
      error: (data) ->
        $('.errors', $this).empty().append $('<div/>').text(data.errors)

    return false

