$(document).delegate '#home-page', 'pageshow', ->
  $('#drop-a-verse-page, #my-verses-page, #nearby-verses-page').remove()

  $('#log-out', $(this)).click ->
    localStorage.clear()
    $.mobile.changePage 'index.html'

