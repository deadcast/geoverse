$(document).delegate '#forgot-password-page', 'pageinit', ->
  $(this).delegate '#forgot-password-form', 'submit', ->
    $this = $(this)
    geoverse.helpers.ajax
      url:  $this.attr('action')
      data:
        email: $('#forgot-password-email', $this).val()
      type: $this.attr('method')
      success: (data) ->
        $.mobile.changePage 'forgot_password_success.html'
      error: (data) ->
        $('.errors', $this).empty().append $('<div/>').text(data.errors)

    return false
