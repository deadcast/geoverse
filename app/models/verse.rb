class Verse
  include Mongoid::Document

  belongs_to :bible

  field :_id, type: String
  field :title, type: String
  field :text, type: String

  validates :title, :text, presence: true
end
