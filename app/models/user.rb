class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword

  has_secure_password
  before_save :downcase_email, :check_if_password_still_forgotten

  has_many :dropped_verses

  field :email, type: String
  field :password_digest, type: String
  field :authentication_token, type: String
  field :has_forgot_password, type: Boolean  
  index({ email: 1, authentication_token: 1 }, { unique: true })

  attr_accessible :email, :password, :password_confirmation, :has_forgot_password
  validates :email, presence: true, uniqueness: true, format: /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i
  validates :password, length: { minimum: 6 }

  def create_and_save_new_auth_token
    new_token = loop do
      token = generate_token
      break token unless User.where(authentication_token: token).exists?
    end
    update_attribute(:authentication_token, new_token)
    new_token
  end

  def create_and_save_new_password
    new_password = generate_token[0..5].downcase
    update_attributes(
      :password => new_password,
      :has_forgot_password => true
    )
    email_new_password(new_password)
    new_password
  end

  def verse_list
    dropped_verses.limit(100).desc(:created_at).collect do |dropped_verse|
      {
        id: dropped_verse.id,
        title: dropped_verse.title,
        text: dropped_verse.text,
        time_ago: dropped_verse.time_ago,
        location: dropped_verse.location
      }
    end
  end

  private

  def check_if_password_still_forgotten
    self.has_forgot_password = false if password && password_confirmation
    true # return true so the save works and doesn't think it failed
  end

  def email_new_password password
    UserMailer.delay.forgot_password_email(
      :email => self.email,
      :new_password => password
    )
  end

  def generate_token
    remove_unsafe_chars(random_string)
  end

  def random_string
    SecureRandom.base64(15)
  end

  def remove_unsafe_chars string
    string.tr('+/=', 'xyz')
  end

  def downcase_email
    self.email = self.email.downcase
  end
end
