class Bible
  include Mongoid::Document

  has_many :verses, autosave: true

  field :_id, type: Integer
  field :name, type: String
  field :book_names, type: Array

  validates :name, :book_names, presence: true

  def self.lookup value
    digits = value.split ':'
    bible = Bible.find(digits.first.to_i)

    case digits.length
      when 1
        { books: bible.book_names }
      when 2
        id = /#{digits[0]}:#{digits[1]}:\d+:1\b/
        { chapter_count: bible.verses.where(_id: id).count }
      when 3
        id = /#{digits[0]}:#{digits[1]}:#{digits[2]}:\d+\b/
        { verse_count: bible.verses.where(_id: id).count }
      when 4
        id = "#{digits[0]}:#{digits[1]}:#{digits[2]}:#{digits[3]}"
        verse = bible.verses.find id
        { title: verse.title, text: verse.text }
    end
  end
end
