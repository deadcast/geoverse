module Mobile::V1::PageLayoutHelper
  def boolean_option value
    value.present? ? value : 'false'
  end

  def page_role_option value
    value.present? ? value : 'page'
  end
end
