RSpec::Matchers.define :have_back_button do |expected|
  match do |actual|
    find(expected[:within])
    actual.body.should have_selector(
      "#{expected[:within]} .ui-header a[data-rel=back] .ui-btn-text",
      :text => 'Back'
    )
  end
end
