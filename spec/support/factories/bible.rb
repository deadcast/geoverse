FactoryGirl.define do
  factory :bible do |bible|
    bible.id 1
    bible.name 'King James Bible'
    bible.book_names ['Genesis', 'Exodus']

    after(:create) do |this|
      FactoryGirl.create(:verse, bible: this, id: '1:1:1:1',  text: 'Verse 1', title: 'Genesis 1:1')
      FactoryGirl.create(:verse, bible: this, id: '1:1:2:1',  text: 'Verse 2', title: 'Genesis 2:1')
      FactoryGirl.create(:verse, bible: this, id: '1:1:2:33', text: 'Verse 4', title: 'Genesis 2:33')
      FactoryGirl.create(:verse, bible: this, id: '1:1:45:1', text: 'Verse 5', title: 'Genesis 45:1')
      FactoryGirl.create(:verse, bible: this, id: '1:2:1:1',  text: 'Verse 6', title: 'Exodus 1:1')
    end
  end
end
