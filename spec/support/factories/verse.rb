FactoryGirl.define do
  factory :verse do |verse|
    verse.id '1:1:1:1'
    verse.title 'Genesis 1:1'
    verse.text 'In the beginning...'
  end
end