FactoryGirl.define do
  factory :user do |user|
    user.email 'test@example.com'
    user.password 'abc123.happy'
    user.authentication_token 'O9Gw9qkukGAHV1KVVxyz'

    after(:create) do |this|
      FactoryGirl.create(:dropped_verse, user: this, verse_id: '1:1:1:1', location: [37, -121], created_at: 2.days.ago)
      FactoryGirl.create(:dropped_verse, user: this, verse_id: '1:1:2:1', location: [34, -120], created_at: 5.minutes.ago)
    end
  end
end
