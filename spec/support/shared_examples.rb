shared_examples 'an authenticated controller' do |actions|
  context 'when signed out' do
    actions.each_pair do |action, verb|
      it 'returns back a 401 response' do
        param_hash = {:auth_token => 'invalid'}
        param_hash.merge!({:id => '1'}) if action == :show
        send(verb, action, param_hash)
        response.status.should == 401
      end
    end
  end
end

shared_examples 'an exported view' do |name|
  it "exports the view" do
    visit mobile_v1_screen_path(name.to_sym)
    if ENV['export_app']
      export_view(page.html, name)
    else
      export_view(page.html, name, :test_version => 'v1')
    end
  end
end

shared_examples 'a map' do |selector|
  let(:header_height) { page.evaluate_script("$('.ui-header:last').height()") }
  let(:window_height) { page.evaluate_script("window.innerHeight") }
  let(:map_height) { page.evaluate_script("$('#dropped-verse-map').height()") }

  before do
    page.has_css? '#dropped-verses-list li', visible: true
    click_on 'Genesis 1:1'
    page.should have_css '#view-on-map-page', :visible => true
  end

  it 'shows the map' do
    page.should have_css '#dropped-verse-map .leaflet-map-pane'
  end

  it 'shows the location of the verse' do
    page.should have_css '.leaflet-marker-pane', :visible => true
  end

  it 'shows a popup of the verse' do
    page.should have_css '.leaflet-popup', :visible => true
    page.should have_selector '.leaflet-popup-content .title', :text => 'Genesis 1:1'
    page.should have_selector '.leaflet-popup-content .text', :text => 'Verse 1'
  end

  it 'shows the back button' do
    page.should have_back_button :within => '#view-on-map-page'
  end

  it 'caches the previous page' do
    page.should have_css selector[:for]
  end

  it 'correctly sets the height of the map' do
    page.has_css? '.leaflet-control-attribution', visible: true
    map_height.should == window_height - header_height
  end
end

