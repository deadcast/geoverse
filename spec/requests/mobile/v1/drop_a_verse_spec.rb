require 'spec_helper'

describe 'drop a verse screen', :js => true do
  before do
    FactoryGirl.create(:bible)

    visit mobile_v1_screen_path :home
    page.execute_script("localStorage.authToken = '#{auth_token}'")

    click_link 'Drop a Verse'
    select 'Genesis', from: 'book-select'
    select '2', from: 'chapter-select'
    select '1', from: 'verse-select'
  end

  let(:auth_token) { FactoryGirl.create(:user).authentication_token }

  def it_should_show_success_message
    page.has_css? '#success-home-button', :visible => true
    page.should have_content 'Your verse was successfully dropped at your location!'
  end

  context 'when dropping a verse' do
    before do
      expect {
        click_on 'drop-button'
        it_should_show_success_message
      }.to change(DroppedVerse, :count)
    end

    it 'removes the page when going back to home' do
      click_on 'success-home-button'
      page.should_not have_css '#drop-a-verse-page'
      page.should have_content 'Home'
    end

    it 'it does not show a back button after dropping a verse' do
      page.should_not have_back_button :within => '#success-page'
    end
  end

  context 'when previewing a verse' do
    before { click_on 'preview-button' }

    it 'lets the user preview a verse before dropping' do
      page.should have_content 'Genesis 2:1'
      page.should have_content 'Verse 2'

      click_on 'preview-back-button'
      page.has_css? '#drop-button', :visible => true

      click_on 'drop-button'
      it_should_show_success_message
    end

    it 'caches the drop a verse page' do
      page.should have_css '#drop-a-verse-page'
    end
  end
end
