require 'spec_helper'
require "#{Rails.root}/lib/set_url_options"

describe 'exporting rendered views' do
  it_behaves_like 'an exported view', 'home'

  it_behaves_like 'an exported view', 'drop_a_verse'

  it_behaves_like 'an exported view', 'forgot_password'

  it_behaves_like 'an exported view', 'forgot_password_success'

  it_behaves_like 'an exported view', 'my_verses'

  it_behaves_like 'an exported view', 'nearby_verses'

  it_behaves_like 'an exported view', 'preview'

  it_behaves_like 'an exported view', 'login'

  it_behaves_like 'an exported view', 'login_no_back'

  it_behaves_like 'an exported view', 'sign_up'

  it_behaves_like 'an exported view', 'drop_a_verse_success'

  it_behaves_like 'an exported view', 'update_password'

  it_behaves_like 'an exported view', 'update_password_success'

  it_behaves_like 'an exported view', 'view_on_map'

  it_behaves_like 'an exported view', 'index'
end

