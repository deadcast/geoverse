require 'spec_helper'

describe 'logout screen', :js => true do
  before do
    visit mobile_v1_screen_path :home
    page.execute_script "localStorage.authToken = '#{auth_token}'"
  end

  let(:auth_token) { FactoryGirl.create(:user).authentication_token }

  context 'when the user clicks the log out button' do
    before { click_link 'Log Out' }

    it 'takes them to the index screen' do
      page.should have_css '#index-page'
      page.should_not have_back_button :within => '#index-page'
    end
  end
end

