require 'spec_helper'

describe 'index screen', :js => true do
  before { visit mobile_v1_screen_path :index }

  context 'when clicking on the log in button' do
    it 'takes you to the log in screen' do
      click_on 'Log In'
      page.should have_content 'Log In'
      page.should have_css '#login-form'
      page.should have_back_button :within => '#login-page'
    end
  end

  context 'when clicking on the sign up button' do
    it 'takes you to the sign up screen' do
      click_on 'Sign Up'
      page.should have_content 'Sign Up'
      page.should have_css '#sign-up-form'
      page.should have_back_button :within => '#sign-up-page'
    end
  end
end
