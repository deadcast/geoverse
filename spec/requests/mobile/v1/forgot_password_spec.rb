require 'spec_helper'

describe 'forgot password', :js => true do
  before do
    visit mobile_v1_screen_path :index
    click_link 'Log In'
    
    page.has_css? '#forgot-password', visible: true
    click_link 'Forgot Password'
  end

  let(:user) { FactoryGirl.create(:user) }
  let(:email) { user.email }

  context 'when clicking on the forgot password button' do
    it 'goes to the forgot password screen' do
      page.should have_css '#forgot-password-page'
    end
  end

  context 'when a users submits a valid email address' do
    before do
      fill_in 'Your email please', :with => email
      click_on 'send-password'
    end

    it 'takes them to the success screen' do
      page.should have_css '#forgot-password-success-page'
      page.should_not have_back_button :within => '#forgot-password-success-page'
    end

    context 'when the user clicks the button to go to the login page' do
      it 'does not show a back button' do
        page.has_css? '#go-to-login', visible: true
        click_on 'Go to Login'
        page.should_not have_back_button :within => '#login-page'
      end
    end
  end

  context 'when the user submits a bogus email' do
    before do
      fill_in 'Your email please', :with => 'cheddar-bunny@yummy.com'
      click_on 'send-password'
    end

    it 'returns back a sad message' do
      page.should have_content "Hmmm, we couldn't find the email address you provided. Try again?"
    end
  end
end
