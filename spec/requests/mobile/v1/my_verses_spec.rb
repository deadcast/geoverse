require 'spec_helper'

describe 'my verses screen', :js => true do
  before do
    FactoryGirl.create(:bible)
    visit mobile_v1_screen_path :home
    page.execute_script("localStorage.clear()")
    page.execute_script("localStorage.authToken = '#{auth_token}'")
    click_link 'My Verses'
  end

  let(:user) { FactoryGirl.create(:user) }
  let(:auth_token) { user.authentication_token }

  context 'viewing the my verses screen' do
    context 'and the user has verses' do
      it 'shows a list of dropped verses' do
        page.should have_content 'My Verses'
        page.should have_selector('#dropped-verses-list li:first-child h1', :text => 'Genesis 2:1')
        page.should have_selector('#dropped-verses-list li:first-child p', :text => 'Dropped 5 minutes ago')
      end
    end

    context 'and the user has no verses' do
      before do
        user.dropped_verses.delete_all
        visit mobile_v1_screen_path :my_verses
      end

      it 'shows an awesome message' do
        page.should have_content "Oh noes! You haven't dropped any verses yet."
        page.should_not have_css '#dropped-verses-list li'
      end
    end
  end

  context 'clicking on a verse' do
    it_behaves_like 'a map', { :for => '#my-verses-page' }
  end
  
  context 'clicking view verses on map button' do
    it 'shows all of my verses' do
      find(:css, '.view-verse-map').click()
      page.should have_css '.leaflet-marker-icon', :visible => true, :count => 2
    end
  end

  context 'going back to the dashboard' do
    it 'removes this page from the dom' do
      click_link 'Back'
      page.should_not have_css '#my-verses-page'
    end
  end
end
