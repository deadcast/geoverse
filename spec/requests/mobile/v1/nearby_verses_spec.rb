require 'spec_helper'

describe 'nearby verses screen', :js => true do
  before do
    FactoryGirl.create(:bible)
    FactoryGirl.create(:dropped_verse)
    DroppedVerse.create_indexes

    visit mobile_v1_screen_path :home
    page.execute_script("localStorage.clear()")
    page.execute_script("localStorage.authToken = '#{auth_token}'")
    click_link 'Nearby Verses'
  end

  let(:user) { FactoryGirl.create(:user) }
  let(:auth_token) { user.authentication_token }
  let(:dropped_verse) { DroppedVerse.where(verse_id: '1:1:1:1').first }

  context 'when viewing the nearby verses screen' do
    context 'and there are nearby verses' do
      it 'shows a list of dropped verses' do
        page.should have_content 'Verses Nearby'
        page.should have_selector('#nearby-verses-list li:first-child h1', :text => 'Genesis 1:1')
        page.should have_selector('#nearby-verses-list li:first-child p.time-ago', :text => 'Dropped less than a minute ago')
        page.should have_selector('#nearby-verses-list li:first-child p.distance', :text => '0 feet')
      end
    end

    context 'and there are no nearby verses' do
      before do
        DroppedVerse.delete_all
        visit mobile_v1_screen_path :nearby_verses
      end

      it 'shows a sad message' do
        page.should have_content "Oh my! Nobody has dropped any verses in your area. Let's fix that!"
        page.should_not have_css '#nearby-verses-list li'
      end
    end
  end

  context 'when clicking on a verse' do
    it_behaves_like 'a map', { :for => '#nearby-verses-page' }
  end

  context 'when going back to the dashboard' do
    it 'removes this page from the dom' do
      click_link 'Back'
      page.should_not have_css '#nearby-verses-page'
    end
  end

  describe 'refreshing' do
    context 'clicking on the refresh button in the header' do
      it 'refreshes the list' do
        page.should have_selector('#nearby-verses-list li:first-child h1', :text => 'Genesis 1:1')
        page.should have_selector('#nearby-verses-list li:first-child p.distance', :text => '0 feet')

        dropped_verse.update_attributes(location: [37, -125.001])
        click_link 'Refresh'

        page.should have_selector('#nearby-verses-list li:first-child h1', :text => 'Genesis 1:1')
        page.should have_selector('#nearby-verses-list li:first-child p.distance', :text => '365 feet')
      end
    end
  end
end

