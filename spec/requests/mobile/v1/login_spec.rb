require 'spec_helper'

describe 'login screen', :js => true do
  let(:user) { FactoryGirl.create(:user) }

  context 'when sucessfully logging in' do
    it 'takes you to the home page' do
      visit mobile_v1_screen_path :login
      fill_in 'email', :with => user.email
      fill_in 'password', :with => user.password
      click_on "Let's go!"
      page.should have_content 'Home'
      page.should_not have_back_button :within => '#home-page'
    end
  end

  context 'when unsuccessfully logging in' do
    it 'shows you an error' do
      visit mobile_v1_screen_path :login
      fill_in 'email', :with => 'kitty@pants.com'
      fill_in 'password', :with => 'wrong'
      click_on "Let's go!"
      page.should have_content "Sorry! We couldn't find your email or password."
    end
  end

  context 'when logging in with an auto generated password' do
    before do
      visit mobile_v1_screen_path :login
      fill_in 'email', :with => user.email
      fill_in 'password', :with => user.password
      click_on "Let's go!"
    end

    let(:user) { FactoryGirl.create(:user, :has_forgot_password => true) }

    it 'goes to the create new password screen' do
      page.should have_css '#update-password-page'
    end

    context 'when the user has updated their password' do
      let(:new_password) { 'asdqwe123' }

      context 'successfully' do
        before do
          fill_in 'update-password', :with => new_password
          fill_in 'update-password-confirmation', :with => new_password
          click_on "Let's do this!"
        end

        it 'shows the success screen' do
          page.should have_css '#update-password-success-page'
          page.should_not have_back_button :within => '#update-password-success-page'
        end

        describe 'logging in again' do
          it 'does not go to the update password page' do
            page.has_css? '#go-to-home', visible: true
            page.click_on 'Go to Home'
            
            page.has_css? '#log-out', visible: true
            page.click_link 'Log Out'
            
            page.has_css? '#log-in', visible: true
            page.click_link 'Log In'

            fill_in 'email', :with => user.email
            fill_in 'password', :with => new_password
            click_on "Let's go!"

            page.should have_css '#home-page'
          end
        end
      end

      context 'unsuccessfully' do
        it 'shows an error' do
          fill_in 'update-password', :with => 'jellyfish'
          click_on "Let's do this!"
          page.should have_content "Password doesn't match confirmation"
        end
      end
    end
  end

  describe 'unauthorized redirect' do
    context 'when visiting a page with ajax' do
      it 'redirects to the log in page' do
        visit mobile_v1_screen_path :home
        click_link 'My Verses'
        page.should have_css '#login-page'
        page.should_not have_back_button within: '#login-page'
      end
    end
  end
end

