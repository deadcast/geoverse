require 'spec_helper'

describe 'sign up screen', :js => true do
  context 'when sucessfully signing up' do
    it 'takes you to the home page' do
      visit mobile_v1_screen_path :sign_up
      fill_in 'email', :with => 'chuck@norris.com'
      fill_in 'password', :with => 'roundhousekick'
      fill_in 'password confirmation', :with => 'roundhousekick'
      click_on 'Sign me up!'
      page.should have_content 'Home'
      page.should_not have_back_button :within => '#home-page'
    end
  end

  context 'when unsuccessfully signing up' do
    it 'shows you an error' do
      visit mobile_v1_screen_path :sign_up
      click_on 'Sign me up!'
      page.should have_content "Email can't be blank"
    end
  end
end

