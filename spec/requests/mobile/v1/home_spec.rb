require 'spec_helper'

describe 'home screen', :js => true do
  before { visit mobile_v1_screen_path :home }

  context 'when clicking on the drop a verse button' do
    before { FactoryGirl.create(:bible) }

    it 'takes you to the drop a verse screen' do
      click_on 'Drop a Verse'
      page.should have_css '#drop-a-verse-page'
      page.should have_back_button :within => '#drop-a-verse-page'
    end
  end

  context 'when clicking on the view my verses button' do
    it 'takes you to the view my verses screen' do
      click_on 'My Verses'
      page.should have_css '#my-verses-page'
      page.should have_back_button :within => '#my-verses-page'
    end
  end

  context 'when clicking on the nearby verses button' do
    it 'takes you to the nearby verses screen' do
      click_on 'Nearby Verses'
      page.should have_css '#nearby-verses-page'
      page.should have_back_button :within => '#nearby-verses-page'
    end
  end
end

