require 'spec_helper'

describe V1::PasswordsController do
  let(:user) { FactoryGirl.create(:user) }

  it_behaves_like 'an authenticated controller', {
    :update => :put
  }

  describe '#create' do
    let(:valid_email) { user.email }
    let(:invalid_email) { 'chicken@nugget.com' }

    describe 'routing' do
      it 'finds the create route' do
        { post: 'v1/password' }.should route_to controller: 'v1/passwords', action: 'create'
      end
    end

    context 'given a valid email address is provided' do
      it 'creates a new password for the user' do
        User.any_instance.expects(:create_and_save_new_password)
        post :create, { email: valid_email }
      end

      it 'returns a success response' do
        post :create, { email: valid_email }
        response.status.should == 200
      end
    end

    context 'given a non-valid email address is provided' do
      it 'does not creates a new password for the user' do
        User.any_instance.expects(:create_and_save_new_password).never
        post :create, { email: invalid_email }
      end

      it 'returns an error response with a message' do
        post :create, { email: invalid_email }
        response.status.should == 422
        json_response = JSON.parse(response.body)
        json_response['errors'].should == "Hmmm, we couldn't find the email address you provided. Try again?"
      end
    end
  end

  describe '#update' do
    let(:request_hash) {
      {
        user: {
          password: new_password,
          password_confirmation: new_password
        },
        auth_token: user.authentication_token
      }
    }

    describe 'routing' do
      it 'finds the update route' do
        { put: 'v1/password' }.should route_to controller: 'v1/passwords', action: 'update'
      end
    end

    context 'given a valid new password is provided' do
      before { user.update_attribute(:has_forgot_password, true) }
      let(:new_password) { 'asdqwe123' }

      it "updates the user's password" do
        expect {
          put :update, request_hash
        }.to change { user.reload.password_digest }
      end

      it 'sets the has_forgot_password attribute to false' do
        expect {
          put :update, request_hash
        }.to change { user.reload.has_forgot_password }.from(true).to(false)
      end

      it 'returns a success response' do
        put :update, request_hash
        response.status.should == 200
      end
    end

    context 'given an invalid new password is provided' do
      let(:new_password) { 'bad' }

      it "does not update the user's password" do
        expect {
          put :update, request_hash
          user.reload
        }.not_to change(user, :password_digest)
      end

      it 'returns an error response' do
        put :update, request_hash
        response.status.should == 422
        json_response = JSON.parse(response.body)
        json_response['errors'].first.should =~ /Password is too short/
      end
    end
  end
end
