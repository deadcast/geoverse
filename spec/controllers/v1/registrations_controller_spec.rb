require 'spec_helper'

describe V1::RegistrationsController do

  subject { FactoryGirl.create(:user) }

  describe '#create' do
    describe 'routing' do
      it 'finds the create route' do
        { post: 'v1/registration' }.should route_to controller: 'v1/registrations', action: 'create'
      end
    end

    describe 'registration' do
      it 'registers a new user' do
        User.any_instance.stubs(:create_and_save_new_auth_token).returns('secret')
        post :create, { :user => { :email => 'chunky@bacon.com', :password => 'abc123', 
          :password_confirmation => 'abc123' } }
        response.status.should == 201
        json_response = JSON.parse(response.body)
        json_response['auth_token'].should == 'secret'
      end

      it 'returns an error when password confirmation does not match' do
        post :create, { :user => { :email => 'chunky@bacon.com', :password => 'abc123', 
          :password_confirmation => 'abc1234' } }
        response.status.should == 422
        json_response = JSON.parse(response.body)
        json_response['errors'].should == ['Password doesn\'t match confirmation']
      end

      it 'returns errors when email and password are blank' do
        post :create, { :user => { :email => '', :password => '', 
          :password_confirmation => '' } }
        response.status.should == 422
        json_response = JSON.parse(response.body)
        json_response['errors'].should == ['Password can\'t be blank', 'Email can\'t be blank', 
          'Email is invalid', 'Password is too short (minimum is 6 characters)']
      end

      it 'returns an error when email address is already taken' do
        post :create, { :user => { :email => subject.email, :password => 'abc123', 
          :password_confirmation => 'abc123' } }
        response.status.should == 422
        json_response = JSON.parse(response.body)
        json_response['errors'].should == ['Email is already taken']
      end
    end
  end
end
