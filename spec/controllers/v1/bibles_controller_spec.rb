require 'spec_helper'

describe V1::BiblesController do
  let(:user) { FactoryGirl.create(:user) }
  let(:auth_token) { user.authentication_token }
  let(:bible) { FactoryGirl.create(:bible) }

  it_behaves_like 'an authenticated controller', {
    :index => :get
  }

  describe '#index' do
    context 'routing' do
      it 'finds the index route' do
        { get: 'v1/bibles' }.should route_to controller: 'v1/bibles', action: 'index'
      end
    end

    context 'when passing a specific verse id' do
      it 'returns back the text and title for the verse' do
        get :index, lookup: "#{bible.id}:1:2:33", auth_token: auth_token
        json_response = JSON.parse(response.body)
        json_response['title'].should == 'Genesis 2:33'
        json_response['text'].should == 'Verse 4'
      end
    end
  end
end

