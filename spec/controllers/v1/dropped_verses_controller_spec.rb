require 'spec_helper'

describe V1::DroppedVersesController do
  before { FactoryGirl.create(:bible) }

  let(:user) { FactoryGirl.create(:user) }
  let(:auth_token) { user.authentication_token }
  let(:dropped_verse) { user.dropped_verses.where(verse_id: '1:1:2:1').first }

  it_behaves_like 'an authenticated controller', {
    :index => :get,
    :show => :get,
    :create => :post
  }

  describe '#index' do
    describe 'routing' do
      it 'finds the index route' do
        { get: 'v1/dropped_verses' }.should route_to controller: 'v1/dropped_verses', action: 'index'
      end
    end

    it 'returns back a json hash of dropped verses' do
      get :index, { auth_token: auth_token }
      json_response = JSON.parse(response.body)
      json_response['verses'][0]['id'].should == dropped_verse.id.to_s
      json_response['verses'][0]['title'].should == 'Genesis 2:1'
      json_response['verses'][0]['text'].should == 'Verse 2'
      json_response['verses'][0]['time_ago'].should == 'Dropped 5 minutes ago'
      json_response['verses'][0]['location'].should == [34, -120]
    end
  end

  describe '#show' do
    describe 'routing' do
      it 'finds the show route' do
        { get: 'v1/dropped_verses/1' }.should route_to controller: 'v1/dropped_verses', action: 'show', id: '1'
      end
    end

    it 'returns back a dropped verse' do
      get :show, { id: dropped_verse.id, auth_token: auth_token }
      json_response = JSON.parse(response.body)
      json_response['title'].should == 'Genesis 2:1'
      json_response['text'].should == 'Verse 2'
      json_response['location'].should == [34, -120]
    end
  end

  describe '#create' do
    context 'routing' do
      it 'finds the create route' do
        { post: 'v1/dropped_verses' }.should route_to controller: 'v1/dropped_verses', action: 'create'
      end
    end

    context 'when dropping a verse' do
      before do
        post :create, lookup: '1:1:2:33', auth_token: auth_token, lat: '37.123', lon: '-121.234'
      end

      it 'returns a 201 response' do
        response.status.should == 201
      end

      it 'saves the verse with the location' do
        dropped_verse = user.dropped_verses.where(verse_id: '1:1:2:33').first
        dropped_verse.location.should == [37.123, -121.234]
      end
    end
  end
end

