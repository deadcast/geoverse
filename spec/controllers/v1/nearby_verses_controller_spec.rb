require 'spec_helper'

describe V1::NearbyVersesController do
  before do
    FactoryGirl.create(:bible)
    FactoryGirl.create(:dropped_verse)
    DroppedVerse.create_indexes
  end

  let(:user) { FactoryGirl.create(:user) }
  let(:auth_token) { user.authentication_token }
  let(:dropped_verse) { DroppedVerse.first }

  it_behaves_like 'an authenticated controller', {
    :index => :get,
    :show => :get
  }

  describe '#index' do
    describe 'routing' do
      it 'finds the index route' do
        { get: 'v1/nearby_verses' }.should route_to controller: 'v1/nearby_verses', action: 'index'
      end
    end

    it 'returns back a json hash of nearby dropped verses' do
      get :index, { auth_token: auth_token, lat: '37', lon: '-125.05' }
      json_response = JSON.parse(response.body)
      json_response['verses'][0]['id'].should == dropped_verse.id.to_s
      json_response['verses'][0]['title'].should == 'Genesis 1:1'
      json_response['verses'][0]['text'].should == 'Verse 1'
      json_response['verses'][0]['time_ago'].should == 'Dropped less than a minute ago'
      json_response['verses'][0]['location'].should == [37, -125]
      json_response['verses'][0]['distance'].should == '3.4 miles'
    end
  end

  describe '#show' do
    describe 'routing' do
      it 'finds the show route' do
        { get: 'v1/nearby_verses/1' }.should route_to controller: 'v1/nearby_verses', action: 'show', id: '1'
      end
    end

    it 'returns back a nearby dropped verse' do
      get :show, { id: dropped_verse.id, auth_token: auth_token }
      json_response = JSON.parse(response.body)
      json_response['title'].should == 'Genesis 1:1'
      json_response['text'].should == 'Verse 1'
      json_response['location'].should == [37, -125]
    end
  end
end

