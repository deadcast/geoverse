require 'spec_helper'

describe V1::ApplicationController do
  let(:user) { FactoryGirl.create(:user) }

  describe '#current_user' do
    context 'when a valid auth token is present' do
      it 'returns the current user' do
        controller.params = { auth_token: user.authentication_token }
        subject.current_user.should == user
      end
    end

    context 'when an invalid auth token is present' do
      it 'returns nil' do
        controller.params = { auth_token: 'pizza' }
        subject.current_user.should be_nil
      end
    end
  end

  describe '#authenticate' do
    after { subject.authenticate }

    context 'when the auth token is found' do
      it "doesn't render anything" do
        subject.stubs(:current_user).returns(true)
        subject.expects(:render).never
      end
    end

    context 'when the auth token is not found' do
      it 'renders a 401 response' do
        subject.stubs(:current_user).returns(false)
        subject.expects(:render).with(
          :json => {}, :status => 401
        )
      end
    end
  end
end
