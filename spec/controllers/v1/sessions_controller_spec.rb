require 'spec_helper'

describe V1::SessionsController do

  subject { FactoryGirl.create(:user) }

  describe '#create' do
    describe 'routing' do
      it 'finds the create route' do
        { post: 'v1/session' }.should route_to controller: 'v1/sessions', action: 'create'
      end
    end

    context 'when logging in' do
      context 'successfully' do
        it 'returns the correct json' do
          User.any_instance.stubs(:create_and_save_new_auth_token).returns('secret')
          post :create, { :email => subject.email, :password => subject.password }
          json_response = JSON.parse(response.body)
          json_response['auth_token'].should == 'secret'
          json_response['forgot_password'].should == false
        end

        it 'makes sure that email case sensitivity does not matter' do
          post :create, { :email => subject.email.upcase, :password => subject.password }
          response.status.should == 200
        end
      end

      context 'unsuccessfully' do
        it 'returns an error when email or password is not found' do
          post :create, { :email => 'kitty', :password => 'pants' }
          response.status.should == 422
          json_response = JSON.parse(response.body)
          json_response['errors'].should == "Sorry! We couldn't find your email or password."
          json_response['auth_token'].should be_blank
        end

        it 'returns an error when email and password are nil' do
          post :create
          response.status.should == 422
        end
      end
    end

    context 'when logging in with a system generated password' do
      before do
        subject.update_attribute(:has_forgot_password, true)
      end

      it 'returns forgot password as true' do
        post :create, { :email => subject.email, :password => subject.password }
        json_response = JSON.parse(response.body)
        json_response['forgot_password'].should == true
      end
    end
  end
end
