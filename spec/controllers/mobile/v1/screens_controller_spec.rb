require 'spec_helper'

describe Mobile::V1::ScreensController do
  context 'layout' do
    it 'has mobile v1 layout' do
      get :index
      response.should render_template 'layouts/mobile_v1'
    end
  end

  context 'routing' do
    it 'generates the correct url' do
      { :get => mobile_v1_screen_path(:index) }.should route_to(
        :controller => 'mobile/v1/screens',
        :action => 'index')
    end
  end
end
