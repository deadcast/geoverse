require 'spec_helper'

describe User do
  subject { FactoryGirl.create(:user) }

  it { should be_mongoid_document }

  it { should be_timestamped_document }

  context 'field types' do
    it { should have_field(:email).of_type(String) }

    it { should have_field(:password_digest).of_type(String) }

    it { should have_field(:authentication_token).of_type(String) }

    it { should have_field(:has_forgot_password).of_type(Boolean) }
  end

  context 'validations' do
    context 'email' do
      it { should validate_format_of(:email).not_to_allow('abc.com') }

      it { should validate_format_of(:email).to_allow('abc@me.com') }

      it { should validate_presence_of :email }

      it { should validate_uniqueness_of :email }

      it 'downcases email upon saving' do
        subject.update_attributes(:email => 'aPPle@carrots.COM')
        subject.email.should == 'apple@carrots.com'
      end
    end

    context 'password' do
      it { should validate_length_of(:password).with_minimum(6) }

      it { should validate_confirmation_of(:password) }
    end

    it { should have_index_for(email: 1, authentication_token: 1).with_options(unique: true) }
  end

  context 'auth token generation' do
    describe '#create_and_save_new_auth_token' do
      it 'generates a new token and saves it' do
        subject.stubs(:generate_token).returns('1hiKSKEl0hRFzqLWjxyz')
        subject.create_and_save_new_auth_token
        subject.authentication_token.should == '1hiKSKEl0hRFzqLWjxyz'
      end

      it 'does not generate and save duplicate tokens' do
        subject.stubs(:generate_token).returns(subject.authentication_token, 'different')
        subject.create_and_save_new_auth_token
        subject.authentication_token.should == 'different'
      end
    end

    describe '#generate_token' do
      it 'returns a token' do
        subject.stubs(:random_string).returns('1hiKSKEl0hRFzqLWj+/=')
        subject.send(:generate_token).should == '1hiKSKEl0hRFzqLWjxyz'
      end
    end

    describe '#random_string' do
      it 'returns a random 20 characters long string' do
        subject.send(:random_string).should have(20).characters
      end
    end

    describe '#remove_unsafe_chars' do
      it 'remove unsafe characters from a string' do
        subject.send(:remove_unsafe_chars, 'abc+/=').should == 'abcxyz'
      end
    end
  end

  context 'relationships' do
    it { should have_many :dropped_verses }
  end

  describe '#verse_list' do
    before { FactoryGirl.create(:bible) }

    let(:dropped_verses) { subject.dropped_verses }
    let(:dropped_verse_1) { dropped_verses.where(verse_id: '1:1:1:1').first }
    let(:dropped_verse_2) { dropped_verses.where(verse_id: '1:1:2:1').first }

    it 'returns back a sorted hash array of verses' do
      subject.verse_list.should == [
        {
          id: dropped_verse_2.id,
          title: 'Genesis 2:1',
          text: 'Verse 2',
          time_ago: 'Dropped 5 minutes ago',
          location: [34, -120]
        },
        {
          id: dropped_verse_1.id,
          title: 'Genesis 1:1',
          text: 'Verse 1',
          time_ago: 'Dropped 2 days ago',
          location: [37, -121]
        }
      ]
    end

    it 'limits the dropped verses to 100' do
      sort_desc_stub = stub('sort', :desc => [])
      dropped_verses.expects(:limit).with(100).returns(sort_desc_stub)
      subject.verse_list
    end
  end

  describe '#create_and_save_new_password' do
    before { subject.stubs(:generate_token).returns('abc123') }

    let(:create_new_password) { subject.create_and_save_new_password }

    it 'creates a new password and saves it' do
      expect {
        create_new_password
      }.to change(subject, :password_digest)
    end

    it 'down cases the new password' do
      subject.stubs(:generate_token).returns('AbcDEF')
      create_new_password.should == 'abcdef'
    end

    it 'makes the new password 6 characters in length' do
      subject.stubs(:generate_token).returns('123456789')
      create_new_password.should have(6).characters
    end

    it 'sends an email to the user' do
      mock_mailer = mock('mailer')
      mock_mailer.expects(:forgot_password_email).with(
        :email => subject.email, :new_password => 'abc123'
      )
      UserMailer.expects(:delay).returns(mock_mailer)
      create_new_password
    end

    it 'sets forgot_password to true' do
      create_new_password
      subject.should have_forgot_password
    end
  end

  context 'when a user updates their forgotten password' do
    it 'sets has_forgot_password to false' do
      subject.update_attributes(
        :password => 'qweasd',
        :password_confirmation => 'qweasd',
        :has_forgot_password => true
      )
      subject.should_not have_forgot_password
    end
  end
end
