require 'spec_helper'

describe Bible do
  it { should be_mongoid_document }

  context 'field types' do
    it { should have_field(:name).of_type(String) }
    it { should have_field(:book_names).of_type(Array) }
  end

  context 'validations' do
    it { should validate_presence_of :name }
    it { should validate_presence_of :book_names }
  end

  context 'relationships' do
    it { should have_many :verses }
  end

  describe '.lookup' do
    before { FactoryGirl.create(:bible) }

    it 'returns back the book names' do
      Bible.lookup('1').should == { books: ['Genesis', 'Exodus'] }
    end

    it 'returns back the chapter count for the book' do
      Bible.lookup('1:1').should == { chapter_count: 3 }
    end

    it 'returns back the verse count for the chapter' do
      Bible.lookup('1:1:2').should == { verse_count: 2 }
    end

    it 'returns back the text and title for the verse' do
      Bible.lookup('1:1:2:33').should == {
        title: 'Genesis 2:33',
        text: 'Verse 4'
      }
    end
  end
end
