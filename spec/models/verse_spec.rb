require 'spec_helper'

describe Verse do
  it { should be_mongoid_document }

  context 'field types' do
    it { should have_field(:title).of_type(String) }
    it { should have_field(:text).of_type(String) }
  end

  context 'validations' do
    it { should validate_presence_of :title }
    it { should validate_presence_of :text }
  end

  context 'relationships' do
    it { should belong_to :bible }
  end
end
