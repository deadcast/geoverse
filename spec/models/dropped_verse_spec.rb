require 'spec_helper'

describe DroppedVerse do
  subject { FactoryGirl.create(:dropped_verse) }

  before { FactoryGirl.create(:verse) }

  it { should be_mongoid_document }

  it { should be_timestamped_document }

  context 'field types' do
    it { should have_field(:location).of_type(Array) }
  end

  context 'validations' do
    it { should have_index_for(location: "2d") }
    it { should validate_presence_of :location }
  end

  context 'relationships' do
    it { should belong_to :user }
    it { should belong_to :verse }
  end

  context 'verse helper methods' do
    describe '#title' do
      it 'returns the title of the verse' do
        subject.title.should == 'Genesis 1:1'
      end
    end

    describe '#text' do
      it 'returns the text of the verse' do
        subject.text.should == 'In the beginning...'
      end
    end

    describe '#time_ago' do
      it 'returns the distance of time when the verse was dropped' do
        subject.time_ago.should == 'Dropped less than a minute ago'
      end
    end
  end

  describe '.nearby' do
    before do
      subject
      DroppedVerse.create_indexes
    end

    context 'there are nearby verses' do
      it 'returns a hash of dropped verses' do
        dropped_verse = DroppedVerse.nearby('37', '-125.05').first
        dropped_verse[:id].should == subject.id
        dropped_verse[:title].should == subject.title
        dropped_verse[:text].should == subject.text
        dropped_verse[:time_ago].should == subject.time_ago
        dropped_verse[:location].should == subject.location
        dropped_verse[:distance].should == '3.4 miles'
      end

      it 'limits the number of nearby verses to 100' do
        DroppedVerse.mongo_session.expects(:command).with(has_entry(:num, 100)).returns('results' => [])
        DroppedVerse.nearby('37', '-125.05')
      end

      describe 'distance formatting' do
        context 'the distance is greater than 0.1 miles' do
          it 'returns the distance in miles' do
            dropped_verse = DroppedVerse.nearby('37', '-125.05').first
            dropped_verse[:distance].should == '3.4 miles'
          end
        end

        context 'the distance is less than 0.1 miles' do
          it 'returns the distance in feet' do
            dropped_verse = DroppedVerse.nearby('37', '-125.001').first
            dropped_verse[:distance].should == '364 feet'
          end
        end

        context 'the distance is 1 mile' do
          it "returns miles as singular" do
            dropped_verse = DroppedVerse.nearby('37', '-125.014').first
            dropped_verse[:distance].should == '1 mile'
          end
        end
      end
    end

    context 'no user location is provided' do
      it 'returns an empty array' do
        DroppedVerse.nearby('', '').should be_empty
      end
    end
  end
end

