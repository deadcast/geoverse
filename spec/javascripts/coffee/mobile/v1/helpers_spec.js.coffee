describe 'helper methods', ->
  describe '#ajax', ->
    request = {}
    success = callback: (data) -> 'nothing'
    error = callback: (data) -> 'nothing'

    itShowsAConnectionErrorForStatus = (status) ->
      beforeEach ->
          window.navigator.network.connection.type = status
          geoverse.helpers.ajax({ url: 'path/to/url', type: 'POST', data: {} })

        it 'shows an error message', ->
          expect(geoverse.helpers.showError).toHaveBeenCalledWith "Sorry, your device is offline. Try again once you're connected."

        it 'does not make an ajax request', ->
          expect($.ajax).not.toHaveBeenCalled()

    beforeEach ->
      localStorage.clear()
      jasmine.Ajax.useMock()

      spyOn(success, 'callback')
      spyOn(error, 'callback')
      spyOn(_, 'defer').andCallThrough()
      spyOn($.mobile, 'showPageLoadingMsg')
      spyOn($.mobile, 'hidePageLoadingMsg')

      localStorage.authToken = 'kittyradish'

    describe 'connected to the internet', ->
      beforeEach ->
        geoverse.helpers.ajax 
          url: 'path/to/url', 
          type: 'POST', 
          data: { kitties: 'are kool' }, 
          success: (data) -> success.callback data
          error: (data) -> error.callback data

        request = mostRecentAjaxRequest()

      it 'calls the correct url', ->
        expect(request.url).toEqual 'path/to/url'

      it 'posts the request', ->
        expect(request.method).toEqual 'POST'

      it 'send an ajax request with correct parameters', ->
        expect(request.params).toEqual $.param({
          kitties: 'are kool', auth_token: 'kittyradish'
        })

      describe 'responses', ->
        describe 'success', ->
          it 'calls the success callback with data', ->
            request.response responseText: JSON.stringify({weather: 'sunny'}), status: 200
            expect(success.callback).toHaveBeenCalledWith weather: 'sunny'

        describe 'error', ->
          it 'calls the error callback with data', ->
            request.response responseText: JSON.stringify({weather: 'cloudy'}), status: 422
            expect(error.callback).toHaveBeenCalledWith weather: 'cloudy'

        describe 'unauthorized', ->
          beforeEach ->
            spyOn($.mobile, 'changePage')
            spyOn(window.localStorage, 'clear')
            request.response status: 401

          it 'changes to the login page', ->
            expect($.mobile.changePage).toHaveBeenCalledWith('login_no_back.html')

          it 'clears out the local storage', ->
            expect(localStorage.clear).toHaveBeenCalled()

        describe 'raised exception', ->
          beforeEach ->
            spyOn geoverse.helpers, 'showError'
            request.response status: 500

          it 'shows an error with the correct message', ->
            expect(geoverse.helpers.showError).toHaveBeenCalledWith 'Oh noes! Something bad just happened.'

      describe 'loading screen', ->
        describe 'when an ajax request is made', ->
          beforeEach -> request.response status: 200

          it 'shows the loading screen', ->
            expect(_.defer).toHaveBeenCalled()
            waits(0)
            runs -> expect($.mobile.showPageLoadingMsg).toHaveBeenCalled()

          it 'hides the loading screen', ->
            expect($.mobile.hidePageLoadingMsg).toHaveBeenCalled()

    describe 'not connected to the internet', ->
      beforeEach ->
        spyOn(geoverse.helpers, 'showError')
        spyOn($, 'ajax')

      describe 'the connection type is none', ->
        itShowsAConnectionErrorForStatus 'none'

      describe 'the connection type is unknown', ->
        itShowsAConnectionErrorForStatus 'unknown'

  describe '#showError', ->
    it 'shows the popup with the correct text', ->
      spyOn($.mobile, 'showPageLoadingMsg')
      geoverse.helpers.showError 'pumpkins'
      expect($.mobile.showPageLoadingMsg).toHaveBeenCalledWith('e', 'pumpkins', true)

  describe '#getLocation', ->
    success = callback: (data) -> 'nothing'
    error = callback: (data) -> 'nothing'

    beforeEach ->
      spyOn(success, 'callback')
      spyOn(error, 'callback')
      spyOn(_, 'defer').andCallThrough()
      spyOn($.mobile, 'showPageLoadingMsg')
      spyOn($.mobile, 'hidePageLoadingMsg')

    describe 'trying to get the users location', ->
      it 'shows the loading message', ->
        window.navigator = geolocation:
          getCurrentPosition: (success, error, options) -> {}
        geoverse.helpers.getLocation(success, error)
        expect(_.defer).toHaveBeenCalled()
        waits(0)
        runs -> expect($.mobile.showPageLoadingMsg).toHaveBeenCalled()

    describe 'the user is successfully located', ->
      beforeEach ->
        window.navigator = geolocation:
          getCurrentPosition: (success, error, options) -> success()
        geoverse.helpers.getLocation(success.callback, error.callback)

      it 'calls the success callback', ->
        expect(success.callback).toHaveBeenCalled()

      it 'hides the loading message', ->
        expect(_.defer).toHaveBeenCalled()
        waits(0)
        runs -> expect($.mobile.hidePageLoadingMsg).toHaveBeenCalled()

    describe 'the user is unsuccessfully located', ->
      beforeEach ->
        window.navigator = geolocation:
          getCurrentPosition: (success, error, options) -> error()
        geoverse.helpers.getLocation(success.callback, error.callback)

      it 'calls the error callback', ->
        expect(error.callback).toHaveBeenCalled()

      it 'hides the loading message', ->
        expect(_.defer).toHaveBeenCalled()
        waits(0)
        runs -> expect($.mobile.hidePageLoadingMsg).toHaveBeenCalled()

    describe 'options', ->
      it 'has high accuracy enabled and caching disabled', ->
        window.navigator = geolocation:
          getCurrentPosition: (success, error, options) ->
            expect(options.enableHighAccuracy).toBeTruthy()
            expect(options.maximumAge).toEqual 0
        geoverse.helpers.getLocation(success.callback, error.callback)

