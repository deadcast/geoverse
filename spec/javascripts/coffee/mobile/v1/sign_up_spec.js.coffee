jasmine.getFixtures().fixturesPath = 'spec/javascripts/fixtures/mobile/v1'

describe 'sign up', ->
  request = {}

  $$$ =
    page: -> $('#sign-up-page')
    form: -> $('#sign-up-form')
    emailField: -> $('#sign-up-email')
    passwordField: -> $('#sign-up-password')
    passwordConfirmationField: -> $('#sign-up-password-confirmation')

  beforeEach ->
    localStorage.clear()
    loadFixtures 'sign_up.html'
    spyOn $.mobile, 'changePage'
    jasmine.Ajax.useMock()
    $$$.page().trigger 'pageinit'

  describe 'when sign up successful', ->
    beforeEach ->
      $$$.emailField().val('test@example.com')
      $$$.passwordField().val('secret')
      $$$.passwordConfirmationField().val('secret')
      $$$.form().submit()

      request = mostRecentAjaxRequest()
      request.response responseText: JSON.stringify({ auth_token: '123456789' })

    it 'sends correct json data to server', ->
      expect(request.params).toEqual $.param user:
        email: 'test@example.com'
        password: 'secret'
        password_confirmation: 'secret'

    it 'calls the correct url', ->
      expect(request.url).toEqual 'v1/registration'

    it 'posts the request', ->
      expect(request.method).toEqual 'POST'

    it 'saves the returned auth token', ->
      expect(localStorage.authToken).toEqual '123456789'

    it 'changes to the home page', ->
      expect($.mobile.changePage).toHaveBeenCalledWith 'home.html'

  describe 'when sign up is not successful', ->
    submitForm = {}
    beforeEach ->
      submitForm = ->
        $('#sign-up-form').submit()
        request = mostRecentAjaxRequest()
        request.response responseText: JSON.stringify({errors: ['email is invalid', 'password can\'t be blank']}), status: 422
      submitForm()

    it 'does not save the returned auth token', ->
      expect(localStorage.authToken).toBeUndefined()

    it 'does not change the page to dashbaord', ->
      expect($.mobile.changePage).not.toHaveBeenCalled()

    it 'shows error message', ->
      expect($('#sign-up-form .errors')).toHaveHtml '<div>email is invalid</div><div>password can\'t be blank</div>'

    it 'does not keep appending errors messages', ->
      submitForm()
      expect($('#sign-up-form .errors')).toHaveHtml '<div>email is invalid</div><div>password can\'t be blank</div>'
