jasmine.getFixtures().fixturesPath = 'spec/javascripts/fixtures/mobile/v1'

describe 'my verses', ->
  request = {}

  $$$ =
    page: -> $('#my-verses-page'),
    droppedVersesList: -> $('#dropped-verses-list'),
    droppedVerses: -> $('li', this.droppedVersesList()),
    viewVerseMapButton: -> $('.view-verse-map', this.page())

  sendRequestAndGetResponse = (hash) ->
    request = mostRecentAjaxRequest()
    request.response responseText: JSON.stringify(hash)

  beforeEach ->
    localStorage.clear()
    loadFixtures 'my_verses.html'
    jasmine.Ajax.useMock()

    spyOn($.mobile, 'loadPage')
    spyOn($.fn, 'listview')

    localStorage.authToken = '123456789'
    $$$.page().trigger 'pageinit'
    sendRequestAndGetResponse verses: [
      { 
        id: '12345', 
        title: 'Exodus 2:1', 
        text: 'Some verse...', 
        time_ago: 'Dropped 1 day ago',
        location: '37,-121'
      },
      { 
        id: '12346', 
        title: 'Exodus 2:2', 
        text: 'Some more verses...', 
        time_ago: 'Dropped 3 days ago',
        location: '37,-125'
      }
    ]

  describe 'page initially loads', ->
    it 'sends an ajax request to get the dropped verses', ->
      expect(request.url).toEqual 'v1/dropped_verses?auth_token=123456789'

    describe 'and the user has verses', ->
      it 'inserts dropped verses into the list', ->
        firstVerse = $(_.first($$$.droppedVerses()))
        expect(firstVerse.find('a.verse-details').attr('data-verse-id')).toEqual '12345'
        expect(firstVerse.find('a h1.title')).toHaveText 'Exodus 2:1'
        expect(firstVerse.find('a p.time-ago')).toHaveText 'Dropped 1 day ago'
        
      it 'shows the view verse map button', ->
        expect($$$.viewVerseMapButton()).toBeVisible()

    describe 'and the user has no verses', ->
      beforeEach ->
        $$$.page().trigger 'pageinit'
        sendRequestAndGetResponse verses: []

      it 'shows a notice that no verses have been dropped', ->
        expect($$$.page().find('.notice')).toHaveText "Oh noes! You haven't dropped any verses yet."
        
      it 'does not show the view verse map button', ->
        expect($$$.viewVerseMapButton()).toBeHidden()

    it 'refreshes the list view', ->
      expect($.fn.listview).toHaveBeenCalledWith 'refresh'

  describe 'clicking on a dropped verse', ->
    beforeEach ->
      spyOn($.mobile, 'changePage')
      $('a', _.last($$$.droppedVerses())).click()

    it 'saves the dropped verse data', ->
      verse = JSON.parse(localStorage.verses)[0]
      expect(verse.title).toEqual 'Exodus 2:2'
      expect(verse.text).toEqual 'Some more verses...'
      expect(verse.location).toEqual '37,-125'

    it 'goes to the details page', ->
      expect($.mobile.changePage).toHaveBeenCalledWith 'view_on_map.html'
      
  describe 'clicking on view verse map', ->
    beforeEach ->
      spyOn($.mobile, 'changePage')
      $$$.viewVerseMapButton().find('a').click()
      
    it 'saves all of the dropped verses', ->
      verses = JSON.parse(localStorage.verses)
      expect(verses.length).toEqual(2)
      
    it 'goes to the details page', ->
      expect($.mobile.changePage).toHaveBeenCalledWith 'view_on_map.html', { transition: 'slideup' }
      
  describe 'the page is hidden', ->
    it 'removes the active style from the navbar button', ->
      $('.view-verse-map a').addClass('ui-btn-active')
      $$$.page().trigger('pagehide')
      expect($('.view-verse-map a')).not.toHaveClass('ui-btn-active')
