jasmine.getFixtures().fixturesPath = 'spec/javascripts/fixtures/mobile/v1'

describe 'dropping a verse', ->
  request = {}

  $$$ =
    page: -> $('#drop-a-verse-page'),
    bookSelect: -> $('#book-select'),
    bookOptions: -> $('option', @bookSelect()),
    chapterSelect: -> $('#chapter-select'),
    chapterOptions: -> $('option', @chapterSelect()),
    verseSelect: -> $('#verse-select'),
    verseOptions: -> $('option', @verseSelect()),
    dropButton: -> $('#drop-button'),
    previewButton: -> $('#preview-button'),
    previewPage: -> $('#preview-page'),
    buttons: -> $('.button-container', @page())

  selectBook = (number, hash) ->
    $$$.bookSelect().val(number).change()
    expect($$$.chapterSelect()).toBeHidden()
    sendRequestAndGetResponse hash

  selectChapter = (number, hash) ->
    $$$.chapterSelect().val(number).change()
    expect($$$.verseSelect()).toBeHidden()
    sendRequestAndGetResponse hash

  selectVerse = (number, hash) ->
    $$$.verseSelect().val(number).change()
    expect($$$.buttons()).toBeHidden()
    sendRequestAndGetResponse hash

  sendRequestAndGetResponse = (hash) ->
    request = mostRecentAjaxRequest()
    request.response responseText: JSON.stringify(hash)

  itDoesNotShowThePreviewOrDropButton = ->
     it 'does not show the preview or drop buttons', ->
      expect($$$.buttons()).toBeHidden()

  itRefreshesTheSelectMenus = ->
    it 'refreshes the select menus', ->
      expect($.fn.selectmenu).toHaveBeenCalledWith 'refresh'

  testSelectOptions = (select, type, options) ->
    expect($(select[0])).toHaveText "Choose a #{type}"
    $.each options, (index, array) ->
      value = if array.length > 1 then [0, 1] else [0, 0]
      expect($(select[index+1])).toHaveValue array[value[0]]
      expect($(select[index+1])).toHaveText array[value[1]]

  beforeEach ->
    localStorage.clear()
    loadFixtures 'drop_a_verse.html'
    jasmine.Ajax.useMock()
    spyOn($.fn, 'selectmenu')

    localStorage.authToken = '123456789'
    $$$.page().trigger 'pageinit'
    
    expect($$$.bookSelect()).toBeHidden()
    sendRequestAndGetResponse books: ['Genesis', 'Exodus', 'Leviticus']

  describe 'when initially viewing the drop a verse form', ->
    it 'sends an ajax request to get the books', ->
      expect(request.url).toEqual 'v1/bibles?lookup=1&auth_token=123456789'

    it 'shows the book select', -> expect($$$.bookSelect()).toBeVisible()

    it 'does not show the chapter select', -> expect($$$.chapterSelect()).toBeHidden()

    it 'does not show the verse select', -> expect($$$.verseSelect()).toBeHidden()

    it 'has the correct length of options', -> expect($$$.bookOptions().length).toEqual 4

    it 'has books in the select menu', ->
      testSelectOptions($$$.bookOptions(), 'Book', [['1', 'Genesis'], ['2', 'Exodus'], ['3', 'Leviticus']])

    itRefreshesTheSelectMenus()

    itDoesNotShowThePreviewOrDropButton()

    describe 'when pageinit is called more than once', ->
      beforeEach ->
        $$$.page().trigger 'pageinit'
        sendRequestAndGetResponse books: ['Genesis', 'Exodus', 'Leviticus']

      it 'has the correct length of book options', -> expect($$$.bookOptions().length).toEqual 4

  describe 'when the user selects a book', ->
    beforeEach -> selectBook 2, chapter_count: '2'

    it 'sends an ajax request to get the chapter count', ->
      expect(unescape(request.url)).toEqual 'v1/bibles?lookup=1:2&auth_token=123456789'

    it 'shows the chapter select', -> expect($$$.chapterSelect()).toBeVisible()

    it 'does not show the verse select', -> expect($$$.verseSelect()).toBeHidden()

    it 'has the correct length of options', -> expect($$$.chapterOptions().length).toEqual 3

    it 'has chapters in the select menu', ->
      testSelectOptions($$$.chapterOptions(), 'Chapter', [['1', '1'], ['2', '2']])

    itRefreshesTheSelectMenus()

    itDoesNotShowThePreviewOrDropButton()

    describe 'when the user selects a chapter', ->
      beforeEach -> selectChapter 2, verse_count: '1'

      it 'sends an ajax request to get the verse count', ->
        expect(unescape(request.url)).toEqual 'v1/bibles?lookup=1:2:2&auth_token=123456789'

      it 'shows the verse select', -> expect($$$.verseSelect()).toBeVisible()

      it 'has the correct length of options', -> expect($$$.verseOptions().length).toEqual 2

      it 'has a verse in the select menu', ->
        testSelectOptions($$$.verseOptions(), 'Verse', [['1', '1']])

      itRefreshesTheSelectMenus()

      itDoesNotShowThePreviewOrDropButton()

      describe 'when the user selects a verse', ->
        beforeEach -> selectVerse 1, title: 'Exodus 2:1', text: "Now a man of the tribe of Levi married..."

        it 'sends an ajax request to get the verse preview', ->
          expect(unescape(request.url)).toEqual 'v1/bibles?lookup=1:2:2:1&auth_token=123456789'

        it 'saves the title and verse text into localStorage', ->
          expect(localStorage.verseTitle).toEqual 'Exodus 2:1'
          expect(localStorage.verseText).toEqual 'Now a man of the tribe of Levi married...'

        it 'shows the preview and drop button', ->
          expect($$$.dropButton()).toBeVisible()
          expect($$$.previewButton()).toBeVisible()

        describe 'when the user clicks the preview button', ->
          beforeEach ->
            spyOn($.mobile, 'changePage')
            $$$.previewButton().click()

          it 'takes them to the preview page', ->
            expect($.mobile.changePage).toHaveBeenCalledWith('preview.html')

        itRefreshesTheSelectMenus()

  describe 'when reselecting a book', ->
    beforeEach ->
      selectBook 3, chapter_count: '2'
      selectChapter 1, verse_count: '2'
      selectVerse 1, {}
      selectBook 2, chapter_count: '3'

    it 'has the correct length of chapter options', -> expect($$$.chapterOptions().length).toEqual 4

    it 'updates the chapter select', ->
      testSelectOptions($$$.chapterOptions(), 'Chapter', [['1', '1'], ['2', '2'], ['3', '3']])

    it 'hides the chapter when selecting the book prompt', ->
      $$$.bookSelect().val('Choose a Book').change()
      expect($$$.chapterSelect()).toBeHidden()

    it 'hides the verse select', -> expect($$$.verseSelect()).toBeHidden()

    itDoesNotShowThePreviewOrDropButton()

  describe 'when reselecting a chapter', ->
    beforeEach ->
      selectBook 2, chapter_count: '3'
      selectChapter 3, verse_count: '5'
      selectVerse 4, {}
      selectChapter 2, verse_count: '1'

    it 'has the correct length of options', -> expect($$$.verseOptions().length).toEqual 2

    it 'updates the verse select', ->
      testSelectOptions($$$.verseOptions(), 'Verse', [['1', '1']])

    it 'hides the verse select when selecting the chapter prompt', ->
      $$$.chapterSelect().val('Choose a Chapter').change()
      expect($$$.verseSelect()).toBeHidden()

    itDoesNotShowThePreviewOrDropButton()

  describe 'when selecting the verse prompt', ->
    beforeEach ->
      selectBook 3, chapter_count: '3'
      selectChapter 1, verse_count: '2'
      selectVerse 1, {}
      selectVerse 'Choose a Verse', {}

    itDoesNotShowThePreviewOrDropButton()

  describe 'when clicking the drop verse button', ->
    beforeEach ->
      selectBook 3, chapter_count: '2'
      selectChapter 2, verse_count: '4'
      selectVerse 4, {}

    describe 'and everything goes smoothly', ->
      beforeEach ->
        window.navigator =
          geolocation:
            getCurrentPosition: (success, error, options) -> success
              coords: { latitude: '37', longitude: '-121' }

        spyOn(navigator.geolocation, 'getCurrentPosition').andCallThrough()
        spyOn($.mobile, 'changePage')
        $$$.dropButton().click()
        sendRequestAndGetResponse status: 'success'

      it "calls the correct geolocation method to get the user's location", ->
        expect(navigator.geolocation.getCurrentPosition).toHaveBeenCalled()

      it 'sends the user location and selected verse', ->
        expect(request.params).toEqual $.param({
          lookup: '1:3:2:4', lat: '37', lon: '-121', auth_token: '123456789'
        })

      it "sends the ajax request as a 'post' type", ->
        expect(request.method).toEqual 'POST'

      it 'sends an ajax request to the correct url', ->
        expect(request.url).toEqual 'v1/dropped_verses'

      it 'goes to the success page', ->
        expect($.mobile.changePage).toHaveBeenCalledWith('drop_a_verse_success.html')

    describe 'and everything does not go smoothly', ->
      beforeEach ->
        window.navigator =
          geolocation:
            getCurrentPosition: (success, error, options) -> error()

        spyOn(navigator.geolocation, 'getCurrentPosition').andCallThrough()
        spyOn($.mobile, 'changePage')
        spyOn(geoverse.helpers, 'showError')
        $$$.dropButton().click()
        sendRequestAndGetResponse status: 'success'

      it 'shows an error when location can not be found', ->
        expect(geoverse.helpers.showError).toHaveBeenCalledWith "We're having trouble finding where you are. Can you try again?"

      it 'does not go to the success page', ->
        expect($.mobile.changePage).not.toHaveBeenCalled()

