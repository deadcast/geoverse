jasmine.getFixtures().fixturesPath = 'spec/javascripts/fixtures/mobile/v1'

describe 'home', ->
  $$$ =
    page: -> $('#home-page')
    logoutButton: -> $('#log-out', this.page())

  expectToBeRemoved = (selector) ->
    expect($(selector).length).toEqual 1
    $$$.page().trigger 'pageshow'
    expect($(selector).length).toEqual 0

  beforeEach ->
    loadFixtures 'home.html'

  describe 'cleaning up', ->
    beforeEach ->
      $$$.page().append($('<div/>').attr('id', 'drop-a-verse-page'))
      $$$.page().append($('<div/>').attr('id', 'my-verses-page'))
      $$$.page().append($('<div/>').attr('id', 'nearby-verses-page'))

    it 'removes the drop a verse page from the dom', ->
      expectToBeRemoved('#drop-a-verse-page')

    it 'removes the my verses page from the dom', ->
      expectToBeRemoved('#my-verses-page')

    it 'removes the nearby verses page from the dom', ->
      expectToBeRemoved('#nearby-verses-page')

  describe 'when clicking log out', ->
    beforeEach ->
      spyOn(window.localStorage, 'clear')
      spyOn($.mobile, 'changePage')
      $$$.page().trigger 'pageshow'
      $$$.logoutButton().click()

    it 'clears out the local storage', ->
      expect(localStorage.clear).toHaveBeenCalled()

    it 'goes to the index screen', ->
      expect($.mobile.changePage).toHaveBeenCalledWith 'index.html'

