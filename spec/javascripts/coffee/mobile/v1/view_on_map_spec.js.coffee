jasmine.getFixtures().fixturesPath = 'spec/javascripts/fixtures/mobile/v1'

describe 'viewing a dropped verse', ->
  $$$ = page: -> $('#view-on-map-page')

  methodCallHistory = []

  fakeMap =
    on: ->
    setView: (location, zoom) ->
      methodCallHistory.push(setView: { location: location, zoom: zoom })
    fitBounds: (bounds) ->
      methodCallHistory.push(fitBounds: {bounds: bounds})
    addLayer: (layer) ->
      methodCallHistory.push(addLayer: { layer: layer })
      fakeMap

  fakeMarker =
    bindPopup: (content, options) ->
      methodCallHistory.push(bindPopup: { content: content, options: options })
      fakeMarker
    openPopup: ->
      methodCallHistory.push(openPopup: { you: 'called me!' })

  beforeEach ->
    localStorage.clear()
    methodCallHistory = []
    loadFixtures 'view_on_map.html'

    spyOn(L, 'Map').andCallFake -> fakeMap
    spyOn(L, 'Marker').andCallFake -> fakeMarker
    spyOn(L, 'TileLayer').andCallFake -> { calling: 'TileLayer' }
    spyOn(L, 'LatLng').andCallFake (lat, lon) -> { lat: lat, lon: lon }

    localStorage.verses = JSON.stringify([
      {
        title: 'Matthew 33:33', 
        text: 'Some verse text for the popup...', 
        location: [34, -125]
      }
    ])

    $$$.page().trigger 'pageshow'

  it 'creates a map with correct id', ->
    expect(L.Map).toHaveBeenCalledWith 'dropped-verse-map'

  it 'creates the correct tile layer with the right properties', ->
    expect(L.TileLayer).toHaveBeenCalledWith(
      window.geoverse.tileServerUrl,
      {
        maxZoom: 18,
        attribution: 'Map data &copy; 2012 OpenStreetMap contributors, Imagery &copy; 2012 CloudMade'
      }
    )

  describe 'displaying a single verse', ->
    it 'finds the verse location', ->
      expect(L.LatLng).toHaveBeenCalledWith(34, -125)

    it 'adds the marker to the right location', ->
      expect(L.Marker).toHaveBeenCalledWith {lat: 34, lon: -125}

    it 'sets the view to the location of the dropped verse', ->
      setViewCalls = _.compact(_.pluck(methodCallHistory, 'setView'))
      expect(_.first(setViewCalls)).toEqual({
        location: { lat: 34, lon: -125 }, zoom: 15
      })

    it 'adds the tile layer and marker layers', ->
      addLayerCalls = _.compact(_.pluck(methodCallHistory, 'addLayer'))
      expect(_.first(addLayerCalls)).toEqual({
        layer: { calling: 'TileLayer' }
      })
      expect(_.last(addLayerCalls)).toEqual(layer: fakeMarker)

    it 'binds the marker popup and then opens it up', ->
      bindPopupCalls = _.compact(_.pluck(methodCallHistory, 'bindPopup'))
      openPopupCalls = _.compact(_.pluck(methodCallHistory, 'openPopup'))

      expect(_.first(bindPopupCalls)).toEqual({
        content: "<b class='title'>Matthew 33:33</b><br/><div class='text'>Some verse text for the popup...</div>"
        options: { maxWidth: 210 }
      })
      expect(_.first(openPopupCalls)).toEqual(you: 'called me!')
      
  describe 'displaying multiple verses', ->
    beforeEach ->
      methodCallHistory = []
      localStorage.verses = JSON.stringify([
        {
          title: 'Matthew 33:33', 
          text: 'Some verse text for the popup...', 
          location: [34, -125]
        },
        {
          title: 'Luke 4:20', 
          text: 'Some dope verse text for the popup...', 
          location: [32, -115]
        }
      ])
      
      L.LatLng.reset()
      spyOn(L, 'LatLngBounds').andCallFake (bounds) -> bounds 
      $$$.page().trigger 'pageshow'
      
    it 'creates the correct number of lat lng points', ->
      expect(L.LatLng.callCount).toEqual(2)
      expect(L.LatLng.argsForCall[0]).toEqual([34, -125])
      expect(L.LatLng.argsForCall[1]).toEqual([32, -115])
      
    it 'creates a lat lng bound object', ->
      expect(L.LatLngBounds).toHaveBeenCalled()
      
    it 'sets the map view to the bounded verses', ->
      setFitBoundsCalls = _.compact(_.pluck(methodCallHistory, 'fitBounds'))
      expect(_.first(setFitBoundsCalls)).toEqual({
        bounds: [{ lat : 34, lon : -125 }, { lat : 32, lon : -115 }]
      })
  
    it 'adds the tile layer and marker layers', ->
      addLayerCalls = _.compact(_.pluck(methodCallHistory, 'addLayer'))
      expect(addLayerCalls[0]).toEqual({
        layer: { calling: 'TileLayer' }
      })
      expect(addLayerCalls[1]).toEqual(layer: fakeMarker)
      expect(addLayerCalls[2]).toEqual(layer: fakeMarker)

    it 'binds the marker popups', ->
      bindPopupCalls = _.compact(_.pluck(methodCallHistory, 'bindPopup'))
      expect(_.first(bindPopupCalls)).toEqual({
        content: "<b class='title'>Matthew 33:33</b><br/><div class='text'>Some verse text for the popup...</div>"
        options: { maxWidth: 210 }
      })
      
      expect(_.last(bindPopupCalls)).toEqual({
        content: "<b class='title'>Luke 4:20</b><br/><div class='text'>Some dope verse text for the popup...</div>"
        options: { maxWidth: 210 }
      })
      
    it 'does not open any of the popups', ->
      openPopupCalls = _.compact(_.pluck(methodCallHistory, 'openPopup'))
      expect(openPopupCalls).toEqual([])
