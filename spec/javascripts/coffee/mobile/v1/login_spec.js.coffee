jasmine.getFixtures().fixturesPath = 'spec/javascripts/fixtures/mobile/v1'

describe 'login', ->
  request = {}

  $$$ = 
    page: -> $('#login-page')
    form: -> $('#login-form')
    emailField: -> $('#login-email')
    passwordField: -> $('#login-password')

  beforeEach ->
    localStorage.clear()
    loadFixtures 'login.html'
    spyOn $.mobile, 'changePage'
    jasmine.Ajax.useMock()
    $$$.page().trigger 'pageinit'

  describe 'when login successful', ->
    beforeEach ->
      $$$.emailField().val('test@example.com')
      $$$.passwordField().val('secret')
      $$$.form().submit()

      request = mostRecentAjaxRequest()
      request.response responseText: JSON.stringify({
        auth_token: '123456789',
        forgot_password: false
      })

    it 'sends correct json data to server', ->
      expect(request.params).toEqual $.param email: 'test@example.com', password: 'secret'

    it 'calls the correct url', ->
      expect(request.url).toEqual 'v1/session'

    it 'posts the request', ->
      expect(request.method).toEqual 'POST'

    it 'saves the returned auth token', ->
      expect(localStorage.authToken).toEqual '123456789'

    it 'changes to the home page', ->
      expect($.mobile.changePage).toHaveBeenCalledWith 'home.html'

  describe 'when log in successful and forgot password is true', ->
    beforeEach ->
      $$$.form().submit()
      request = mostRecentAjaxRequest()
      request.response responseText: JSON.stringify({
        auth_token: '123456789',
        forgot_password: true
      })

    it 'changes the page to update password', ->
      expect($.mobile.changePage).toHaveBeenCalledWith 'update_password.html'

  describe 'when log in is not successful', ->
    submitForm = {}
    beforeEach ->
      submitForm = ->
        $$$.form().submit()
        request = mostRecentAjaxRequest()
        request.response responseText: JSON.stringify({ errors: 'email or password not found' }), status: 422
      submitForm()

    it 'does not save the returned auth token', ->
      expect(localStorage.authToken).toBeUndefined()

    it 'does not change the page to dashbaord', ->
      expect($.mobile.changePage).not.toHaveBeenCalled()

    it 'shows error message', ->
      expect($('.errors', $$$.form())).toHaveHtml '<div>email or password not found</div>'

    it 'does not keep appending errors messages', ->
      submitForm()
      expect($('.errors', $$$.form())).toHaveHtml '<div>email or password not found</div>'

