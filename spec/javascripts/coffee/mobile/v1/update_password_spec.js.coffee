jasmine.getFixtures().fixturesPath = 'spec/javascripts/fixtures/mobile/v1'

describe 'update password', ->
  request = {}

  $$$ =
    page: -> $('#update-password-page')
    form: -> $('#update-password-form', this.page())
    passwordField: -> $('#update-password', this.page())
    passwordConfirmationField: -> $('#update-password-confirmation', this.page())
    errors: -> $('.errors', this.form())

  beforeEach ->
    localStorage.clear()
    loadFixtures 'update_password.html'
    spyOn $.mobile, 'changePage'
    jasmine.Ajax.useMock()

    localStorage.authToken = 'asdqwe123'
    $$$.page().trigger 'pageinit'

  describe 'when a valid password is given', ->
    beforeEach ->
      $$$.passwordField().val('qweasd')
      $$$.passwordConfirmationField().val('qweasd')
      $$$.form().submit()

      request = mostRecentAjaxRequest()
      request.response status: 200

    it 'sends correct json data to server', ->
      expect(request.params).toEqual($.param({
        user: {
          password: 'qweasd', 
          password_confirmation: 'qweasd'
        },
        auth_token: 'asdqwe123'
      }))

    it 'calls the correct url', ->
      expect(request.url).toEqual 'v1/password'

    it 'posts the request', ->
      expect(request.method).toEqual 'PUT'

    it 'changes to the success page', ->
      expect($.mobile.changePage).toHaveBeenCalledWith 'update_password_success.html'

  describe 'when an invalid password is given', ->
    submitForm = {}
    errorMessages = ['password is too short', 'password does not match']
    errorString = "<div>#{errorMessages[0]}</div><div>#{errorMessages[1]}</div>"

    beforeEach ->
      submitForm = ->
        $$$.form().submit()
        request = mostRecentAjaxRequest()
        request.response responseText: JSON.stringify({ errors: errorMessages }), status: 422
      submitForm()

    it 'does not change the page to success page', ->
      expect($.mobile.changePage).not.toHaveBeenCalled()

    it 'shows an error message', ->
      expect($$$.errors()).toHaveHtml errorString

    it 'does not keep appending errors messages', ->
      submitForm()
      expect($$$.errors()).toHaveHtml errorString

