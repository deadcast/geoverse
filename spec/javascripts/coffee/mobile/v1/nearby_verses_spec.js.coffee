jasmine.getFixtures().fixturesPath = 'spec/javascripts/fixtures/mobile/v1'

describe 'nearby verses', ->
  request = {}
  response = { 
    id: '12345', 
    title: 'Exodus 2:1', 
    text: 'Some verse...', 
    time_ago: 'Dropped 1 day ago', 
    location: [37, -121], 
    distance: '0.1 miles' 
  }

  $$$ =
    page: -> $('#nearby-verses-page'),
    droppedVersesList: -> $('#nearby-verses-list'),
    droppedVerses: -> $('li', this.droppedVersesList())

  sendRequestAndGetResponse = (hash) ->
    request = mostRecentAjaxRequest()
    request.response responseText: JSON.stringify(hash)

  itBehavesLikeARefreshAction = (action) ->
    it 'clears the status', ->
      $$$.page().find('.notice').html('hello pluto')
      action()
      expect($$$.page().find('.notice')).toBeEmpty()

    it 'clears the dropped verses', ->
      $$$.droppedVersesList().html('dropped verses')
      action()
      expect($$$.droppedVersesList()).toBeEmpty()

    it 'causes the dropped verses nearby to refresh', ->
      action()
      sendRequestAndGetResponse verses: [response]
      expect($.fn.listview).toHaveBeenCalledWith 'refresh'

    it 'only binds the refresh button once', ->
      spyOnAjax = spyOn(geoverse.helpers, 'ajax')
      _(2).times -> action()
      expect(spyOnAjax.callCount).toEqual(2)

    it 'only binds the li links in the dropped verse list once', ->
      spyOnAjax = spyOn(geoverse.helpers, 'ajax').andCallThrough()
      _(2).times ->
        action()
        sendRequestAndGetResponse verses: [response]
      $('a:first', $$$.droppedVerses()).click()
      expect(spyOnAjax.callCount).toEqual(2)

  triggerResumeEvent = ->
    resumeEvent = document.createEvent 'Events'
    resumeEvent.initEvent 'resume'
    document.dispatchEvent resumeEvent

  beforeEach ->
    localStorage.clear()
    loadFixtures 'nearby_verses.html'
    jasmine.Ajax.useMock()

    spyOn($.mobile, 'loadPage')
    spyOn($.mobile, 'changePage')
    spyOn($.fn, 'listview')
    spyOn(geoverse.helpers, 'showError')

    localStorage.authToken = '123456789'

  describe 'the page initially loads', ->
    describe 'the user can be located', ->
      beforeEach ->
        window.navigator =
          geolocation:
            getCurrentPosition: (success, error, options) -> success
              coords: { latitude: '37', longitude: '-121' }

        $$$.page().trigger 'pageinit'
        sendRequestAndGetResponse verses: [response]

      it 'sends an ajax request to get the dropped verses nearby', ->
        expect(request.url).toEqual 'v1/nearby_verses?lat=37&lon=-121&auth_token=123456789'

      describe 'and there are nearby verses', ->
        it 'inserts dropped verses into the list', ->
          firstVerse = $(_.first($$$.droppedVerses()))
          expect(firstVerse.find('a.verse-details').attr('data-verse-id')).toEqual '12345'
          expect(firstVerse.find('a h1.title')).toHaveText 'Exodus 2:1'
          expect(firstVerse.find('a p.time-ago')).toHaveText 'Dropped 1 day ago'
          expect(firstVerse.find('a p.distance')).toHaveText '0.1 miles'

      describe 'and there are no nearby verses', ->
        beforeEach ->
          $$$.page().trigger 'pageinit'
          sendRequestAndGetResponse verses: []

        it 'shows a notice that no verses have been dropped in the area', ->
          expect($$$.page().find('.notice')).toHaveText "Oh my! Nobody has dropped any verses in your area. Let's fix that!"

      it 'refreshes the list view', ->
        expect($.fn.listview).toHaveBeenCalledWith 'refresh'

    describe 'the user cannot be located', ->
      beforeEach ->
        window.navigator =
          geolocation:
            getCurrentPosition: (success, error, options) -> error()

        $$$.page().trigger 'pageinit'

      it 'shows an error popup', ->
        expect(geoverse.helpers.showError).toHaveBeenCalledWith "We're having trouble finding where you are. Can you try again?"

      it 'shows an error notice', ->
        expect($$$.page().find('.notice')).toHaveText "Oh my! Nobody has dropped any verses in your area. Let's fix that!"

  describe 'refreshing', ->
    beforeEach ->
      window.navigator =
        geolocation:
          getCurrentPosition: (success, error, options) -> success
            coords: { latitude: '35', longitude: '-120' }

    describe 'clicking the refresh arrow', ->
      itBehavesLikeARefreshAction(
        -> $$$.page().find('.refresh').click()
      )

    describe 'resuming the mobile device from sleep', ->
      itBehavesLikeARefreshAction(
        -> triggerResumeEvent()
      )

      describe 'on the view on map page', ->
        it 'does not trigger a refresh', ->
          spyOn($.fn, 'trigger')
          $$$.page().hide()
          triggerResumeEvent()
          expect($.fn.trigger).not.toHaveBeenCalled()

  describe 'clicking on a dropped verse', ->
    beforeEach ->
      window.navigator =
        geolocation:
          getCurrentPosition: (success, error, options) -> success
            coords: { latitude: '37', longitude: '-121' }
      
      $$$.page().trigger 'pageinit'
      sendRequestAndGetResponse verses: [response]
      $('a', _.first($$$.droppedVerses())).click()

    it 'saves the dropped verse data', ->
      verse = JSON.parse(localStorage.verses)[0]
      expect(verse.title).toEqual 'Exodus 2:1'
      expect(verse.text).toEqual 'Some verse...'
      expect(verse.location).toEqual [37, -121]

    it 'goes to the details page', ->
      expect($.mobile.changePage).toHaveBeenCalledWith 'view_on_map.html'

