describe 'initializers', ->
  describe 'namespace', ->
    it 'creates a geoverse namespace', ->
      expect(window.geoverse).not.toBeUndefined()
