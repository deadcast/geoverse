jasmine.getFixtures().fixturesPath = 'spec/javascripts/fixtures/mobile/v1'

describe 'forgot password', ->
  request = {}
  errorMessage = "Hmmm, we couldn't find the email address you provided. Try again?"

  $$$ =
    page: -> $('#forgot-password-page')
    form: -> $('#forgot-password-form', this.page())
    emailField: -> $('#forgot-password-email')
    errors: -> $('.errors', this.form())

  beforeEach ->
    localStorage.clear()
    loadFixtures 'forgot_password.html'
    spyOn $.mobile, 'changePage'
    jasmine.Ajax.useMock()
    $$$.page().trigger 'pageinit'

  describe 'when the email given is found', ->
    beforeEach ->
      $$$.emailField().val('pizza@pillow.com')
      $$$.form().submit()

      request = mostRecentAjaxRequest()
      request.response status: 200

    it 'sends correct json data to server', ->
      expect(request.params).toEqual $.param email: 'pizza@pillow.com'

    it 'calls the correct url', ->
      expect(request.url).toEqual 'v1/password'

    it 'posts the request', ->
      expect(request.method).toEqual 'POST'

    it 'changes to the home page', ->
      expect($.mobile.changePage).toHaveBeenCalledWith 'forgot_password_success.html'

  describe 'when the email given is not found', ->
    submitForm = {}
    beforeEach ->
      submitForm = ->
        $$$.form().submit()
        request = mostRecentAjaxRequest()
        request.response responseText: JSON.stringify({ errors: errorMessage }), status: 422
      submitForm()

    it 'does not change the page to success page', ->
      expect($.mobile.changePage).not.toHaveBeenCalled()

    it 'shows an error message', ->
      expect($$$.errors()).toHaveHtml "<div>#{errorMessage}</div>"

    it 'does not keep appending errors messages', ->
      submitForm()
      expect($$$.errors()).toHaveHtml "<div>#{errorMessage}</div>"


