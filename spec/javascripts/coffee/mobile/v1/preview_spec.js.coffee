jasmine.getFixtures().fixturesPath = 'spec/javascripts/fixtures/mobile/v1'

describe 'previewing a verse', ->
  $$$ = page: -> $('#preview-page')

  beforeEach ->
    localStorage.clear()
    loadFixtures 'preview.html'

    localStorage.verseTitle = 'Exodus 2:1'
    localStorage.verseText = 'Now a man of the tribe of Levi married...'
    $$$.page().trigger 'pageinit'

  describe 'when the page initially loads', ->
    it 'loads the dom with data from localStorage', ->
      expect($('h1.title', $$$.page())).toHaveText 'Exodus 2:1'
      expect($('p.text', $$$.page())).toHaveText "Now a man of the tribe of Levi married..."

