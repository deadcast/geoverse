require "spec_helper"

describe UserMailer do
  describe '#forgot_password_email' do
    let(:email) { UserMailer.forgot_password_email(email_params).deliver }
    let(:email_params) { { :email => 'round@square.com', :new_password => '123456' } }

    context 'when sending the email' do
      it 'goes to the correct user' do
        email.to.should == ['round@square.com']
      end

      it 'comes from the correct address' do
        email.from.should == ['hello@geoverseapp.com']
      end

      it 'has the correct subject' do
        email.subject.should == 'Your shiny new password!'
      end

      it 'contains the correct body' do
        email.encoded.should =~ /Your new password is 123456/
      end
    end
  end
end
