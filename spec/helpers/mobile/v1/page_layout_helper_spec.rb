require 'spec_helper'

describe Mobile::V1::PageLayoutHelper do
  describe '#boolean_option' do
    it 'returns back the given value' do
      helper.boolean_option('pizza').should == 'pizza'
    end

    it 'returns back false when nothing given' do
      helper.boolean_option('').should == 'false'
    end
  end

  describe '#page_role_option' do
    it 'returns back the given value' do
      helper.page_role_option('dialog').should == 'dialog'
    end

    it 'returns back default when nothing given' do
      helper.page_role_option('').should == 'page'
    end
  end
end
