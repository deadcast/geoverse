ENV["RAILS_ENV"] ||= 'test'

require 'simplecov'
SimpleCov.start { coverage_dir 'spec/coverage' }

require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require "#{Rails.root}/lib/view_exporter"
require 'database_cleaner'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  config.mock_with :mocha
  
  config.include Capybara::DSL
  config.include Mongoid::Matchers
  
  config.before :suite do
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) { DatabaseCleaner.start }
  config.after(:each) { DatabaseCleaner.clean }

  config.include ViewExporter
end
