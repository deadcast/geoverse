require 'spec_helper'
require "#{Rails.root}/lib/view_exporter"

class FakeViewExporter
  include ViewExporter
end

describe FakeViewExporter do
  let(:html) { "<body>\n<h1>hello</h1>\n</body>" }
  let(:file_path) { "#{directory}/poor-pluto.html" }
  let(:file_contents) { File.open(file_path).read.strip }

  after do
    FileUtils.rm_rf(directory)
    File.directory?(directory).should be_false
  end

  context 'when rendering the full page' do
    let(:directory) { "#{Rails.root}/tmp/exported_views" }
    before { subject.export_view(html, 'poor-pluto') }

    it 'outputs the correct html to the file' do
      file_contents.should == html
    end

    context 'when a url contains /assets' do
      let(:html) { "<head><script src='/assets/app.js'></script></head>" }

      it 'removes the beginning forward slash' do
        file_contents.should =~ /[^\/]assets\/app\.js/
      end
    end
  end

  context 'when rendering just the partial' do
    let(:directory) { "#{Rails.root}/spec/javascripts/fixtures/mobile/v1" }
    before { subject.export_view(html, 'poor-pluto', :test_version => 'v1') }

    it 'outputs the correct html to the file' do
      file_contents.should == "<h1>hello</h1>"
    end

    context 'when a url contains www.example.com' do
      let(:html) { "<body><a href=\"http://www.example.com/v1/bacon\">hello pluto</a></body>" }
      it 'removes it' do
        file_contents.should == "<a href=\"v1/bacon\">hello pluto</a>"
      end
    end

    context 'when an element has the data-role="page" attribute' do
      let(:html) { "<body><div data-role=\"page\" id=\"something\"></div></body>" }
      it 'removes it' do
        file_contents.should == "<div id=\"something\"></div>"
      end
    end
  end
end

