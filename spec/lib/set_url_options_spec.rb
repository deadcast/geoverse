require 'spec_helper'
require "#{Rails.root}/lib/set_url_options"

describe ActionController::Base do
  after { ENV['export_app'] = nil }

  context 'when export_app is not present' do
    it 'returns an empty hash' do
      subject.default_url_options.should == {}
    end
  end

  context 'when export_app is set to staging' do
    it 'returns back the staging host' do
      ENV['export_app'] = 'staging'
      subject.default_url_options.should == {
        :host => 'geoverse-api-staging.herokuapp.com',
        :protocol => 'https'
      }
    end
  end

  context 'when export_app is set to anything' do
    it 'returns back the production host' do
      ENV['export_app'] = 'production'
      subject.default_url_options.should == {
        :host => 'api.geoverseapp.com',
        :protocol => 'https'
      }
    end
  end
end
