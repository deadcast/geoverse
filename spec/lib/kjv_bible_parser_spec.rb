require 'spec_helper'
require "#{Rails.root}/lib/kjv_bible_parser"

describe KjvBibleParser do
  let(:file_path) { "#{Rails.root}/spec/fixtures/books" }

  let(:books) {[
    { :file => "#{file_path}/genesis.txt", :name => 'Genesis'},
    { :file => "#{file_path}/john.txt", :name => 'John'}
  ]}

  context 'when parsing the kjv bible' do
    it 'opens the book text files' do
      file_stub = stub('file', :read => 'spicy chicken')
      2.times do |i|
        File.expects(:open).with(books[i][:file]).returns(file_stub)
      end
      KjvBibleParser.new(books).run
    end

    it 'creates a log file' do
      Logger.expects(:new).with('log/kjv_bible_parser.log')
      KjvBibleParser.new(books)
    end

    it 'logs a success when finished parsing a book' do
      2.times do |i|
        Logger.any_instance.expects(:info).with("Successfully parsed and saved #{books[i][:name]}")
      end
      KjvBibleParser.new(books).run
    end

    context 'when saving verses' do
      let(:bible) { Bible.find 1 }
      before { KjvBibleParser.new(books).run }

      it 'saves the bible with the correct name' do
        bible.name.should == 'King James Version'
      end

      it 'saves the name of the books' do
        bible.book_names.should == ['Genesis', 'John']
      end

      it 'saves the verses with the correct id' do
        bible.verses.map(&:id).should == ['1:1:1:1', '1:1:1:2', '1:1:1:3',
          '1:1:1:4', '1:1:1:5', '1:2:5:3']
      end

      it 'saves the correct title of the verse' do
        bible.verses.map(&:title).should == ['Genesis 1:1', 'Genesis 1:2', 'Genesis 1:3',
          'Genesis 1:4', 'Genesis 1:5', 'John 5:3']
      end

      it 'saves the text of the verse' do
        bible.verses.map(&:text).should == [
          'In the beginning',
          'Some other: verse here',
          'Look! another verse',
          'And another.',
          'You parsed me!',
          'Then He traveled into a town'
        ]
      end
    end

    context 'when a book file is not found' do
      let(:books) { [{ :file => 'cookies.txt', :name => 'yummy' }] }
      it 'logs an error' do
        Logger.any_instance.expects(:error).with('Could not open cookies.txt')
        KjvBibleParser.new(books).run
      end
    end
  end
end
