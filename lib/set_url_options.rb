module SetUrlOptions
  class ActionController::Base
    def default_url_options
      {}.tap do |params|
        if env = ENV['export_app']
          if env == 'staging'
            params[:host] = "geoverse-api-staging.herokuapp.com"
          else
            params[:host] = "api.geoverseapp.com"
          end
          params[:protocol] = 'https'
        end
      end
    end
  end
end

