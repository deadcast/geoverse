class KjvBibleParser
  attr_accessor :bible, :books, :logger

  def initialize(books_hash)
    self.books = books_hash
    self.logger = Logger.new 'log/kjv_bible_parser.log'
    self.bible = Bible.new :id => BIBLES[:kjv][:id], :name => BIBLES[:kjv][:name]
  end

  def run
    bible.book_names = [].tap do |names|
      books.each_with_index do |book, index|
        names << book[:name]
        if File.exists?(book[:file])
          file = File.open(book[:file])
          bible.verses << [].tap do |verses|
            file.read.scan(/\s*\{\s*(\d+)\s*:\s*(\d+)\s*\}\s*([^\{\}]+)\s*/).each do |chapter, verse, text|
              verses << Verse.new({
                id: "#{BIBLES[:kjv][:id]}:#{index+1}:#{chapter}:#{verse}",
                title: "#{book[:name]} #{chapter}:#{verse}",
                text: text.squish
              })
            end
          end
          logger.info "Successfully parsed and saved #{book[:name]}"
        else
          logger.error "Could not open #{book[:file]}"
        end
      end
    end
    bible.save
  end
end

