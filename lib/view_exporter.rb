module ViewExporter
  def export_view(html, name, options={})
    fixture_path = if options[:test_version]
      File.join(Rails.root, "/spec/javascripts/fixtures/mobile/#{options[:test_version]}")
    else
      File.join(Rails.root, '/tmp/exported_views')
    end

    Dir.mkdir(fixture_path) unless File.exists?(fixture_path)

    fixture_file = File.join(fixture_path, "#{name}.html")
    File.open(fixture_file, 'w') do |file|
      if options[:test_version]
        file.puts(extract_partial(html))
      else
        file.puts(fix_asset_paths(html))
      end
    end
  end

  def extract_partial(html)
    partial = /<body>(.+)<\/body>/m.match(html)[1]
    partial = partial.gsub(/http:\/\/www.example.com\//, '')
    partial.gsub(/\sdata-role="page"/, '')
  end

  def fix_asset_paths(html)
    html.gsub(/\/assets\//, 'assets/')
  end
end

